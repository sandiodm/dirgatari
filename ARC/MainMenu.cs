﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Media;
using System.Windows.Controls;
using System.Windows.Shapes;
using Coding4Fun.Kinect.Wpf.Controls;

namespace ARC
{
    class MainMenu
    {
        public List<Rectangle> rect = new List<Rectangle>();
        private List<Rectangle> iconLevel = new List<Rectangle>();
        //0 = Mainkan; 1 = Tutorial; 2 = Credit;  3 = exit; 4 = back; 5 = Left; 6 = Right;  7 = next; 8 = mudah; 9 = susah
        public List<HoverButton> butt = new List<HoverButton>();
        private double windowWidth;
        private double windowHeight;
        private Canvas canvas;
        //0 = Menu; 1 = pilih lagu; 2 = tutor; 3 = tutor; 
        public int menu;
        private List<Models.SongList> songlist = new List<Models.SongList>();
        public List<Models.SongViewer> sv = new List<Models.SongViewer>();
        public int SongChooser { get; set; }
        public bool changeMusicState;
        public string fileName;
        public List<double> kordinat = new List<double>();
        public int countCalibrate { get; set; }
        public Rectangle badanKalibrasi { get; set; }
        public int counter { get; set; }
        public bool latihan;


        public MainMenu(Canvas _canvas, double _windowWidth, double _windowHeight)
        {
            menu = 0;
            this.latihan = false;
            this.changeMusicState = false;
            this.canvas = _canvas;
            this.windowHeight = _windowHeight;
            this.windowWidth = _windowWidth;
            this.addMenuWindow();
            this.SongChooser = 0;
        }

        private void addMenuWindow()
        {
            this.addButton();
        }

        private void addButton()
        {
            //Mainkan Button
            HoverButton button = new HoverButton();
            button.ImageSource = "/Images/Sprite/mainkan.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 122;
            button.Height = 45;
            Canvas.SetLeft(button, windowWidth);
            Canvas.SetTop(button, (windowHeight/2) - 130);
            this.butt.Add(button);
            canvas.Children.Add(butt[0]);
            Rectangle _rect = new Rectangle { Width = 47, Height = 45 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Icon Mulai.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, windowWidth);
            Canvas.SetTop(_rect, (windowHeight / 2) - 130);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            rect.Add(_rect);
            _rect.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(_rect), (windowWidth - 40) - button.Width - _rect.Width, new Duration(TimeSpan.FromSeconds(1))));
            button.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(button), (windowWidth - 40) - button.Width  , new Duration(TimeSpan.FromSeconds(1))));
            //Tutorial Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/tutor.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 146;
            button.Height = 45;
            Canvas.SetLeft(button, windowWidth);
            Canvas.SetTop(button, (windowHeight / 2) - 40);
            this.butt.Add(button);
            canvas.Children.Add(butt[1]);
            _rect = new Rectangle { Width = 46, Height = 45 };
            brsh = new ImageBrush();
            bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Icon Latihan.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, windowWidth );
            Canvas.SetTop(_rect, (windowHeight / 2) - 40);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            rect.Add(_rect);
            _rect.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(_rect), (windowWidth + 20) - button.Width , new Duration(TimeSpan.FromSeconds(1))));
            button.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(button), (windowWidth - 80) - button.Width - _rect.Width, new Duration(TimeSpan.FromSeconds(1))));
            //Credit Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/creds.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 234;
            button.Height = 45;
            Canvas.SetLeft(button, windowWidth);
            Canvas.SetTop(button, (windowHeight / 2) + 50);
            this.butt.Add(button);
            canvas.Children.Add(butt[2]);
            _rect = new Rectangle { Width = 47, Height = 45 };
            brsh = new ImageBrush();
            bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Icon Tentang Kami.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, windowWidth);
            Canvas.SetTop(_rect, (windowHeight / 2) + 50);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            rect.Add(_rect);
            _rect.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(_rect), (windowWidth - 100) - button.Width - _rect.Width, new Duration(TimeSpan.FromSeconds(1))));
            button.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(button), (windowWidth - 100) - button.Width, new Duration(TimeSpan.FromSeconds(1))));
            ////Exit Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/exit.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 133;
            button.Height = 45;
            Canvas.SetLeft(button, windowWidth);
            Canvas.SetTop(button, (windowHeight / 2) + 140);
            this.butt.Add(button);
            canvas.Children.Add(butt[3]);
            _rect = new Rectangle { Width = 47, Height = 45 };
            brsh = new ImageBrush();
            bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Icon Keluar.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, windowWidth);
            Canvas.SetTop(_rect, (windowHeight / 2) + 140);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            rect.Add(_rect);
            _rect.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(_rect), (windowWidth - 105) - button.Width, new Duration(TimeSpan.FromSeconds(1))));
            button.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(button), (windowWidth - 190) - button.Width - _rect.Width, new Duration(TimeSpan.FromSeconds(1))));
            //Kembali Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/tbl_kembali.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 152;
            button.Height = 45;
            Canvas.SetLeft(button, (windowWidth / 2) - 140 - button.Width);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            //Left
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/Left.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 110;
            button.Height = 85;
            Canvas.SetLeft(button, (windowWidth / 2) - button.Width - 20);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            //Right
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/right.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 110;
            button.Height = 85;
            Canvas.SetLeft(button, (windowWidth / 2) + 20);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            //Next Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/tbl_lanjut.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 133;
            button.Height = 45;
            Canvas.SetLeft(button, (windowWidth / 2) + 140);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            //Mudah Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/Mudah.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 133;
            button.Height = 44;
            Canvas.SetLeft(button, (windowWidth / 2) - 50 - button.Width);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            _rect = new Rectangle { Width = 46, Height = 44 };
            brsh = new ImageBrush();
            bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Mudah icon.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, (windowWidth / 2) - 100 - button.Width);
            Canvas.SetTop(_rect, -200);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            iconLevel.Add(_rect);
            //Susah Button
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/Susah.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 133;
            button.Height = 44;
            Canvas.SetLeft(button, (windowWidth / 2) + 50);
            Canvas.SetTop(button, -200);
            this.butt.Add(button);
            _rect = new Rectangle { Width = 46, Height = 44 };
            brsh = new ImageBrush();
            bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Susah icon.png", UriKind.Relative));
            brsh.ImageSource = bmi;
            Canvas.SetLeft(_rect, (windowWidth / 2) + 50 + button.Width);
            Canvas.SetTop(_rect, -200);
            _rect.Fill = brsh;
            canvas.Children.Add(_rect);
            iconLevel.Add(_rect);
            
        }

        public int NextStep(List<double> scale, Rectangle _rect)
        {
            if (menu == 0)
            {
                //Next Step
                if (scale[0] == 1)
                {
                    menu = 1;
                    for (int i = 0; i < 4; i++)
                    {
                        Canvas.SetLeft(butt[i], windowWidth);
                    }
                    foreach (Rectangle icon in rect)
                    {
                        canvas.Children.Remove(icon);
                    }
                    canvas.Children.Clear();
                    rect.RemoveRange(0, rect.Count);
                    this.PilihMusik();
                    canvas.Children.Add(_rect);
                    return 1;
                }
                //Tutorial
                else if (scale[1] == 1)
                {
                    this.latihan = true;
                    this.fileName = "training";
                    canvas.Children.Clear();
                    return 2;
                }
                //Credits
                else if (scale[2] == 1)
                {
                    canvas.Children.Clear();
                    this.tentangKami();
                    return 3;
                }
                //Exit
                else if (scale[3] == 1)
                {
                    Application.Current.Shutdown();
                }
            }
            else if (menu == 3)
            {
                if (scale[8] >= 1)
                {
                    Canvas.SetLeft(butt[8], windowWidth);
                    Canvas.SetLeft(butt[9], windowWidth);
                    canvas.Children.Clear();
                    countCalibrate = 0;
                    return 1;
                    
                }
                else if (scale[9] >= 1)
                {
                    Canvas.SetLeft(butt[8], windowWidth);
                    Canvas.SetLeft(butt[9], windowWidth);
                    canvas.Children.Clear();
                    countCalibrate = 0;
                    return 2;
                }
            }
            return 0;
        }

        private void tentangKami()
        {
            Rectangle _rect = new Rectangle { Width = 401, Height = 481 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Tentang kami.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, (windowWidth / 2) - (_rect.Width / 2));
            Canvas.SetTop(_rect, (windowHeight / 2) - (_rect.Height / 2));
            canvas.Children.Add(_rect);
            Canvas.SetLeft(butt[4], (windowWidth / 2) + 20);
            Canvas.SetTop(butt[4], Canvas.GetTop(_rect) + 60);
            canvas.Children.Add(butt[4]);
        }

        private void PilihLevel()
        {
            Rectangle _rect = new Rectangle { Width = 464, Height = 197 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Tulisan kesulitan.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, (windowWidth / 2) - (_rect.Width / 2));
            Canvas.SetTop(_rect, ((windowHeight / 2) - (_rect.Height / 2)) - 150);
            canvas.Children.Add(_rect);
            canvas.Children.Add(butt[8]);
            canvas.Children.Add(butt[9]);
            canvas.Children.Add(iconLevel[0]);
            canvas.Children.Add(iconLevel[1]);
            iconLevel[0].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(iconLevel[0]), (windowHeight / 2) , new Duration(TimeSpan.FromSeconds(1))));
            iconLevel[1].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(iconLevel[1]), (windowHeight / 2) , new Duration(TimeSpan.FromSeconds(1))));
            butt[8].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(butt[8]), (windowHeight / 2) , new Duration(TimeSpan.FromSeconds(1))));
            butt[9].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(butt[9]), (windowHeight / 2) , new Duration(TimeSpan.FromSeconds(1))));
        }

        public int NextStep(List<double> scale)
        {
            if (menu == 1)
            {
                //Back Step
                if (scale[4] == 1)
                {
                    return 4;
                }
                //Left
                else if (scale[5] >= 0.5)
                {
                    return 2;
                }
                //Right
                else if (scale[6] >= 0.5)
                {
                    return 3;
                }
                //Next Kalibrasi
                else if (scale[7] == 1)
                {
                    menu = 3;
                    canvas.Children.Clear();
                    for (int i = 4; i < 8; i++)
                    {
                        Canvas.SetLeft(butt[i], windowWidth);
                    }
                    for (int i = 0; i < sv.Count; i++)
                    {
                        Canvas.SetLeft(sv[i].rect, windowWidth);
                        Canvas.SetLeft(sv[i].txt, windowWidth);
                    }
                    this.fileName = sv[SongChooser].txt.Text;
                    countCalibrate = 0;
                    this.PilihLevel();
                    return 1;
                }
            }
            else if (menu == 4)
            {
                if (scale[4] >= 1)
                {
                    return 1;
                }
            }
            return 0;
        }

        public void changeMusic()
        { 
            Canvas.SetLeft(sv[SongChooser].rect, ((windowWidth / 2) - 129));
            Canvas.SetTop(sv[SongChooser].rect, (windowHeight / 2) - 165);
            Canvas.SetLeft(sv[SongChooser].txt, Canvas.GetLeft(sv[SongChooser].rect) + 5);
            Canvas.SetLeft(sv[SongChooser].nilai, (((windowWidth / 2) - 125) + (sv[SongChooser].rect.Width / 2) - 15));
            Canvas.SetTop(sv[SongChooser].nilai, ((windowHeight / 2) - 165) + sv[SongChooser].rect.Height - 130);
            ScaleTransform scaly = new ScaleTransform(1, 1, 100, 50);
            sv[SongChooser].rect.RenderTransform = scaly;
            sv[SongChooser].txt.RenderTransform = scaly;
            if (SongChooser > 0)
            {
                Canvas.SetLeft(sv[SongChooser - 1].rect, ((windowWidth / 2) - 129) - 258);
                Canvas.SetTop(sv[SongChooser - 1].rect, (windowHeight / 2) - 165);
                Canvas.SetLeft(sv[SongChooser - 1].txt, Canvas.GetLeft(sv[SongChooser - 1].rect) + 5);
                Canvas.SetLeft(sv[SongChooser - 1].nilai, windowWidth);
                scaly = new ScaleTransform(0.5, 0.5, 100, 50);
                sv[SongChooser - 1].rect.RenderTransform = scaly;
                sv[SongChooser - 1].txt.RenderTransform = scaly;
            }
            if (SongChooser < sv.Count - 1)
            {
                Canvas.SetLeft(sv[SongChooser + 1].rect, ((windowWidth / 2) - 129) + 278);
                Canvas.SetTop(sv[SongChooser + 1].rect, (windowHeight / 2) - 165); 
                Canvas.SetLeft(sv[SongChooser + 1].txt, Canvas.GetLeft(sv[SongChooser + 1].rect) + 5);
                Canvas.SetLeft(sv[SongChooser + 1].nilai, windowWidth);
                scaly = new ScaleTransform(0.5, 0.5, 100, 50);
                sv[SongChooser + 1].rect.RenderTransform = scaly;
                sv[SongChooser + 1].txt.RenderTransform = scaly;
            }
            if (SongChooser > 1)
            {
                for (int i = SongChooser - 2; i >= 0; i--)
                {
                    Canvas.SetLeft(sv[i].rect, -300);
                    Canvas.SetTop(sv[i].rect, (windowHeight / 2) - 165);
                    Canvas.SetLeft(sv[i].txt, -300);
                    Canvas.SetLeft(sv[i].nilai, -300);
                    scaly = new ScaleTransform(0.5, 0.5, 100, 50);
                    sv[i].rect.RenderTransform = scaly;
                    sv[i].txt.RenderTransform = scaly;
                }
            }

            if(SongChooser < sv.Count - 2)
            {
                for (int i = SongChooser + 2 ; i <= sv.Count - 1; i++)
                {
                    Canvas.SetLeft(sv[i].rect, windowWidth);
                    Canvas.SetTop(sv[i].rect, (windowHeight / 2) - 165);
                    Canvas.SetLeft(sv[i].txt, windowWidth);
                    Canvas.SetLeft(sv[i].nilai, windowWidth);
                    scaly = new ScaleTransform(0.5, 0.5, 100, 50);
                    sv[i].rect.RenderTransform = scaly;
                    sv[i].txt.RenderTransform = scaly;
                }
            }
        }

        public void setKordinat(double dataX, double dataY)
        {
            kordinat.Add(dataX);
            kordinat.Add(dataY);
        }

        private void PilihMusik() 
        {
            Rectangle _rect = new Rectangle { Width = windowWidth + 30, Height = 270 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\bar_musik.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, -15);
            Canvas.SetTop(_rect, (windowHeight / 2) - 200);
            canvas.Children.Add(_rect);
            for (int i = 4; i < 8; i++)
            {
                canvas.Children.Add(butt[i]);
                if (i == 4)
                {
                    butt[i].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(butt[4]), (windowHeight / 2) + 80, new Duration(TimeSpan.FromSeconds(1))));
                }
                else if (i == 7)
                {
                    butt[i].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(butt[4]), (windowHeight / 2) + 80, new Duration(TimeSpan.FromSeconds(1))));
                }
                else 
                {
                    butt[i].BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(butt[4]), (windowHeight / 2) + 60, new Duration(TimeSpan.FromSeconds(1))));
                }
                
            }

            this.readSongList();
        }

        private void readSongList()
        {

            string[] _songListHelper = System.IO.File.ReadAllLines(@"SongFile/SongList.csv");
            string[] _nilai = System.IO.File.ReadAllLines(@"SongFile/nilai.txt");
            int j = 0;
            foreach (string kata in _songListHelper)
            {
                Models.SongList _songlist = new Models.SongList(kata,Int32.Parse(_nilai[j]));
                songlist.Add(_songlist);
                j++;
            }

            for (int i = 0; i < songlist.Count; i++)
            {
                Rectangle _rect = new Rectangle { Width = 258, Height = 208 };
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\bg_songName.png", UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                _rect.Fill = brsh;
                TextBox txt = new TextBox();
                txt.FontSize = 20;
                txt.FontFamily = new FontFamily("Stencil");
                txt.Text = songlist[i].songName.ToString();
                txt.Background = Brushes.Transparent;
                txt.BorderBrush = Brushes.Transparent;
                txt.FontStyle = FontStyles.Oblique;
                TextBox highScore = new TextBox();
                highScore.FontSize = 40;
                highScore.FontFamily = new FontFamily("Stencil");
                highScore.Text = songlist[i].nilai.ToString();
                highScore.Background = Brushes.Transparent;
                highScore.BorderBrush = Brushes.Transparent;
                if (i < 2)
                {
                    Canvas.SetLeft(_rect, ((windowWidth / 2) - 129) + (278 * i));
                    Canvas.SetTop(_rect, (windowHeight / 2) - 165);
                    Canvas.SetLeft(txt, ((windowWidth / 2) - 125) + (278 * i));
                    Canvas.SetTop(txt, (windowHeight / 2) - 160);
                    Canvas.SetLeft(highScore, (Canvas.GetLeft(_rect) + (_rect.Width/2) - 20) + (278 * i) );
                    Canvas.SetTop(highScore, Canvas.GetTop(_rect) + (_rect.Height/2) - 30);
                }
                else 
                {
                    Canvas.SetLeft(_rect, windowWidth);
                    Canvas.SetTop(_rect, (windowHeight / 2) - 165);
                    Canvas.SetLeft(txt, windowWidth);
                    Canvas.SetTop(txt, (windowHeight / 2) - 160);
                    Canvas.SetLeft(highScore, windowWidth);
                    Canvas.SetTop(highScore, ((windowHeight / 2) - 165) + _rect.Height);
                }
                if (i > 0)
                {
                    ScaleTransform scaly = new ScaleTransform(0.5, 0.5 , 100, 50);
                    _rect.RenderTransform = scaly;
                    txt.RenderTransform = scaly;
                }
                sv.Add(new Models.SongViewer(_rect, txt, highScore));
                canvas.Children.Add(sv[i].rect);
                canvas.Children.Add(sv[i].txt);
                canvas.Children.Add(sv[i].nilai);
            }
        }

        public void addInfo()
        {
            Rectangle _rect = new Rectangle { Width = 407, Height = 79 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\perhatian_kalibrasi.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, (windowWidth / 2) - (_rect.Width / 2));
            Canvas.SetTop(_rect, 150);
            canvas.Children.Add(_rect);
        }

        public void badan()
        {
            Rectangle _rect = new Rectangle { Width = 400, Height = 530 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\posisi_player.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, (windowWidth / 2) - (_rect.Width / 2));
            Canvas.SetTop(_rect, 250);
            canvas.Children.Add(_rect);
            badanKalibrasi = _rect;
        }

    }
}
