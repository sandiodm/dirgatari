﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace ARC.Models
{
    class SongViewer
    {
        public Rectangle rect { get; set; }
        public TextBox txt { get; set; }
        public TextBox nilai { get; set; }

        public SongViewer(Rectangle _rect, TextBox _txt, TextBox _nilai)
        {
            this.rect = _rect;
            this.txt = _txt;
            this.nilai = _nilai;
        }

    }
}
