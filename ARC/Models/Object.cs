﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using WPFSoundVisualizationLib;
using System.Windows.Media;

namespace ARC.Models
{
    class Object
    {
        private Rectangle rect;
        private double dropTime;
        private double coorX;
        private double jointX;
        private double coorY;
        private double jointY;
        private int intersect;
        private int kontrol;

        public Object(Rectangle _rect, double _Time, int _kontrol)
        {
            this.rect = _rect;
            this.dropTime = _Time + 500;
            coorX = 0;
            coorY = 0;
            this.intersect = 0;
            this.kontrol = _kontrol;
        }

        public Rectangle getRect
        {
            get { return this.rect; }
        }

        public double getDropTime
        {
            get { return this.dropTime; }
        }

        public double CoorX
        {
            get { return this.coorX; }
            set { jointX = value; coorX = value; }
        }

        public double CoorY
        {
            get { return this.coorY; }
            set { jointY = value; coorY = value - 20; }
        }

        public int Intersect
        {
            get { return this.intersect; }
            set { this.intersect = value; }
        }

        public int Kontrol
        {
            get { return this.kontrol; }
            set { this.kontrol = value; }
        }

    }
}
