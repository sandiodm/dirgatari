﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using WPFSoundVisualizationLib;
using System.Windows.Media;


namespace ARC.Helpers
{
    class ObjectHandler
    {
        private List<Models.Music> musik = new List<Models.Music>();
        private Rectangle rect;
        private String dir;
        private Random rnd = new Random();

        public ObjectHandler(List<Models.Music> _musik)
        {
            this.musik = _musik;
        }

        public Rectangle drawRect() 
        {
            rect = new Rectangle{ Width = 50, Height = 50};
            return rect;
        }

        public int setY(int shoulder, int hip, int kneeRight, int footRight, int id)
        {
            //KONTROL 1 ---- tangan Kanan
            if (this.musik[id].Kontrol == 1)
            {
                return rnd.Next(shoulder / 2, hip);
            }

            // KONTROL 2 ---- tangan Kiri
            else if (this.musik[id].Kontrol == 2)
            {
                return rnd.Next(shoulder / 2, hip);
            }

            // KONTROL 3 ---- Kaki Kanan
            else if (this.musik[id].Kontrol == 3)
            {
                return rnd.Next(kneeRight + 30, footRight);
            }

            // KONTROL 4 ---- Kaki Kiri
            else if (this.musik[id].Kontrol == 4)
            {
                return rnd.Next(kneeRight + 30, footRight);
            }
            return 0;
        }

        public int setX(int shoulderCenter,int elbowRight, int elbowLeft, int kneeLeft, int kneeRight, int id)
        {
            //KONTROL 1 ---- tangan Kanan
            if (this.musik[id].Kontrol == 1)
            {
                return rnd.Next(elbowRight + 30, elbowRight + (elbowRight - shoulderCenter));
            }
            // KONTROL 2 ---- tangan Kiri
            else if (this.musik[id].Kontrol == 2)
            {
                return rnd.Next(elbowLeft - (shoulderCenter - elbowLeft), elbowLeft - 30);
            }
            // KONTROL 3 ---- Kaki Kanan
            else if (this.musik[id].Kontrol == 3)
            {
                return rnd.Next(kneeRight - 30, kneeRight + (kneeRight - shoulderCenter));
            }
            // KONTROL 4 ---- Kaki kiri
            else if (this.musik[id].Kontrol == 4)
            {
                return rnd.Next(kneeLeft - (shoulderCenter - kneeLeft), kneeLeft - 30);
            }
            return 0;
        }

        public string getKontrol1Texture(int id)
        {
            this.dir = Directory.GetCurrentDirectory() + "\\Images\\Sprite";
            if (this.musik[id].Kontrol == 1)
            {
                return dir + "\\Tangan_Kanan.png";
            }
            else if (this.musik[id].Kontrol == 2)
            {
                return dir + "\\tangan_kiri.png";
            }
            else if (this.musik[id].Kontrol == 3)
            {
                return dir + "\\kaki_kanan.png";
            }
            else if (this.musik[id].Kontrol == 4)
            {
                return dir + "\\kaki_kiri.png";
            }
            return dir;
        }

        public void SpawnObject(Joint joint1, Joint joint2, int counter)
        {
            Joint updatedJoint = new Joint();
            if (this.musik[counter].Kontrol < 2)
            {
                updatedJoint = joint1;
                updatedJoint.TrackingState = JointTrackingState.Tracked;
                updatedJoint.Position = CenterPosition(joint1);
            }
            else if (this.musik[counter].Kontrol > 1)
            {
                updatedJoint = joint2;
                updatedJoint.TrackingState = JointTrackingState.Tracked;
                updatedJoint.Position = CenterPosition(joint2);
            }

        }

        private Microsoft.Kinect.SkeletonPoint CenterPosition(Joint joint)
        {
            Microsoft.Kinect.SkeletonPoint vector = new Microsoft.Kinect.SkeletonPoint();
            vector.X = ScaleVector(1024, joint.Position.X);
            vector.Y = ScaleVector(768, -joint.Position.Y);
            vector.Z = ScaleVector(1600, joint.Position.Z);
            return vector;
        }

        private float ScaleVector(int length, float position)
        {
            float value = (((((float)length) / 1f) / 2f) * position) + (length / 2);
            if (value > length)
            {
                return (float)length;
            }
            if (value < 0f)
            {
                return 0f;
            }
            return value;
        }

        public double getNextObjectTimer(int objectCounter)
        {
            return musik[objectCounter].Time;
        }
    }
}
