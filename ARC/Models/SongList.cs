﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARC.Models
{
    public class SongList
    {
        public string songName { get; set; }
        public int nilai { get; set; }

        public SongList(string _songName, int _nilai)
        {
            songName = _songName;
            this.nilai = _nilai;
        }
    }
}
