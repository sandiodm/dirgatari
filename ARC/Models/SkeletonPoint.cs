﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using WPFSoundVisualizationLib;
using System.Windows.Media;

namespace ARC.Models
{
    class SkeletonPoint
    {
        private int id;
        private Ellipse elipse;

        public SkeletonPoint(int _id, Ellipse _elipse)
        {
            this.id = _id;
            this.elipse = _elipse;
        }

        public int ID 
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public Ellipse Elipse
        {
            get { return this.elipse; }
            set { this.elipse = value; }
        }

    }
}
