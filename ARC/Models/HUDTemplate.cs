﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ARC.Models
{
    class HUDTemplate
    {
        private Rectangle frame;
        private double posX;
        private double posY;
        private Rectangle status;
        private double scaleSize;

        public HUDTemplate(Rectangle _frame, double _posX, double _posY)
        {
            this.frame = _frame;
            this.posX = _posX;
            this.posY = _posY;
        }

        public HUDTemplate(Rectangle _status, double _scaleSize)
        {
            this.status = _status;
            this.scaleSize = _scaleSize;
        }

        public HUDTemplate()
        { 
        
        }

        public Rectangle Status
        {
            get { return status; }
        }

        public double ScaleSize
        {
            get { return scaleSize; }
            set { scaleSize = value; }
        }

        public Rectangle Frame
        {
            get { return frame; }
        }

        public double PosX
        {
            get { return posX; }
            set { posX = value; }
        }

        public double PosY
        {
            get { return posY; }
            set { posY = value; }
        }

    }
}
