﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARC.Models
{
    class HealthBar
    {
        private double bar;

        public HealthBar(double _bar)
        {
            this.bar = _bar;
        }

        public double Bar
        {
            get { return bar; }
            set { this.bar = value; }
        }

    }
}
