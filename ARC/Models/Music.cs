﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WPFSoundVisualizationLib;
using Microsoft.Win32;
using System.Xml.Serialization;

namespace ARC.Models
{
    [Serializable]
    [XmlRoot("Foo")]
    public class Music
    {
        private int id;
        private double time;
        private int kontrol;
        private int set;
        private bool valueSet;
        private int kontrol2;
        private int posisi;
        Random rnd = new Random();

        public Music(int _id, double _time, int _kontrol, int _set)
        {
            this.id = _id;
            this.time = _time;
            this.kontrol = _kontrol;
            this.posisi = posisiSet(_kontrol);
            this.set = _set;
            if (_set == 2) { this.valueSet = true; }
            else { valueSet = false; }
            if (valueSet == true) { this.kontrol2 = peluangSet(_kontrol); }
            else { kontrol2 = -1; }
        }

        public Music() {}

        private int posisiSet(int ktrl) 
        {
            if (ktrl == 1)
            {
                return rnd.Next(0, 3);
            }
            else if (ktrl == 2)
            {
                return rnd.Next(3, 6);
            }
            else if (ktrl == 3)
            {
                return 6;
            }
            else if (ktrl == 4)
            {
                return 7;
            }
            return 0;
        }

        private int peluangSet(int _kontrol)
        {
            int _kontrol2 = rnd.Next(1, 5);
            if (_kontrol2 == _kontrol) 
            {
                _kontrol2 -= 1;
                if (_kontrol2 == 0) { _kontrol2 = 4; }
            }
            return _kontrol2;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public double Time
        {
            get { return time; }
            set { time = value; }
        }

        public int Kontrol
        {
            get { return kontrol; }
            set { kontrol = value; }
        }

        public int Set
        {
            get { return set; }
            set { set = value; }
        }

        public bool ValueSet
        {
            get { return valueSet; }
            set { valueSet = value; }
        }

        public int Kontrol2
        {
            get { return kontrol2; }
            set { kontrol2 = value; }
        }

        public int Posisi
        {
            get { return posisi; }
            set { posisi = value; }
        }
    }
}
