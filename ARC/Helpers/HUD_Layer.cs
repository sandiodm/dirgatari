﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace ARC.Helpers
{
    class HUD_Layer
    {
        List<Models.HUDTemplate> template = new List<Models.HUDTemplate>();
        public List<Models.HUDTemplate> status = new List<Models.HUDTemplate>();
        //0 - 3 = alat gerak; 4 - 7 = loadingBar; 8 = hand
        public List<Rectangle> bound = new List<Rectangle>();
        //Khusus bound 5 - 8
        //0 = Mainkan; 1 = Tutorial; 2 = Credit;  3 = exit; 4 = back; 5 = Left; 6 = Right;  7 = next; 8 = mudah; 9 = susah
        public List<double> scale = new List<double>(); 
        //1 shoulder, 2 hip, foot left, foot right
        public List<double> kordinat = new List<double>();

        private double windowWidth;
        private double windowHeight;
        public Models.HealthBar bar;
        private Image barImage;
        public List<Models.HUDTemplate> miss = new List<Models.HUDTemplate>();

        public HUD_Layer(int level, Image _barImage) 
        {
            for(int i = 0; i < 9; i++)
            {
                this.drawBoundingHand(i);
            }
            for (int i = 0; i < 10; i++)
            {
                scale.Add(0);
            }
        }
        public HUD_Layer(int level, Image _barImage, List<double> _kordinat, double _windowHeight, double _windowWidth)
        {
            this.windowHeight = _windowHeight;
            this.windowWidth = _windowWidth;
            this.kordinat = _kordinat;
            bar = new Models.HealthBar(5000);
            this.barImage = _barImage;
            if (level == 1)
            {
                for (int i = 0; i < 6; i++)
                {
                    Rectangle rect = this.data();
                    this.inserting(rect, i);
                }
            }
            else if (level == 2)
            {
                for (int i = 0; i < 8; i++)
                {
                    Rectangle rect = this.data();
                    this.inserting(rect, i);
                }
            }
            for (int i = 0; i < 9; i++)
            {
                this.drawBoundingHand(i);
            }
            for (int i = 0; i < 10; i++)
            {
                scale.Add(0);
            }
        }

        public HUD_Layer()
        {
            bar = new Models.HealthBar(5000);
            for (int i = 0; i < 9; i++)
            {
                this.drawBoundingHand(i);
            }
            for (int i = 0; i < 10; i++)
            {
                scale.Add(0);
            }
        }

        public List<Models.HUDTemplate> Template
        {
            get { return template; }
            set { template = value; }
        }

        protected void drawBoundingHand(int i) 
        {
            if (i < 4)
            {
                Rectangle rect = new Rectangle { Width = 30, Height = 30 };
                rect.Visibility = System.Windows.Visibility.Hidden;
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(getBoundTexture(i), UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                bound.Add(rect);
            }
            else if (i == 8)
            {
                Rectangle rect = new Rectangle { Width = 50, Height = 70 };
                rect.Visibility = System.Windows.Visibility.Hidden;
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\pointer.png", UriKind.Relative));
                brsh.ImageSource = bmi;
                rect.Fill = brsh;
                bound.Add(rect);
            }
            else if( i > 3 && i < 8 )
            {
                Rectangle rect = new Rectangle { Width = 30, Height = 10 };
                rect.Visibility = System.Windows.Visibility.Hidden;
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(getBoundTexture(i), UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                ScaleTransform scaly = new ScaleTransform(0, 1, 0, 0);
                rect.RenderTransform = scaly;
                Canvas.SetLeft(rect, 300);
                Canvas.SetTop(rect, i * 70);
                bound.Add(rect);
            }
        }

        private string getBoundTexture(int i)
        {
            if (i < 4)
            {
                string dir = Directory.GetCurrentDirectory() + "\\Images\\Sprite";
                return dir + "\\bound_hand.png";
            }
            else 
            {
                return Directory.GetCurrentDirectory() + "\\Images\\Sprite\\okBar.png";
            }
        }

        private void inserting(Rectangle _rect, int counter)
        {
            Models.HUDTemplate _temp = new Models.HUDTemplate();
            if (counter == 0) 
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) - ((kordinat[3] - kordinat[1]) * 2) - 10), kordinat[1] - 100);
            }
            else if (counter == 1) 
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) - ((kordinat[3] - kordinat[1]) * 2) - 40), kordinat[1]);
            }
            else if (counter == 2)
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) - ((kordinat[3] - kordinat[1]) * 2) - 10), kordinat[1] + 100);
            }
            else if (counter == 3)
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) + ((kordinat[3] - kordinat[1]) * 2) - 60), kordinat[1] - 100);
            }
            else if (counter == 4)
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) + ((kordinat[3] - kordinat[1]) * 2) - 30), kordinat[1]);
            }
            else if (counter == 5)
            {
                _temp = new Models.HUDTemplate(_rect, ((kordinat[0]) + ((kordinat[3] - kordinat[1]) * 2) - 60), kordinat[1] + 100);
            }
            else if (counter == 6)
            {
                _temp = new Models.HUDTemplate(_rect, kordinat[4] - 150, kordinat[5] - 90);
            }
            else if (counter == 7)
            {
                _temp = new Models.HUDTemplate(_rect, kordinat[6] + 50, kordinat[7] - 100);
            }
            template.Add(_temp);
        }

        private Rectangle data() 
        {
            Rectangle rect = new Rectangle { Width = 80, Height = 80 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(getEdgeTexture(), UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            rect.Fill = brsh;
            return rect;
        }

        private string getEdgeTexture()
        {
            string dir = Directory.GetCurrentDirectory() + "\\Images\\Sprite";
            return dir + "\\object_edge.png";
        }

        public Rect boundingBox(Rectangle rect)
        {
            return new Rect(Canvas.GetLeft(rect), Canvas.GetTop(rect), rect.Width, rect.Height);
        }

        public void Perfect(double x, double y, Canvas canvas, double width , double height)
        {
            Rectangle rect = new Rectangle { Width = 375, Height = 113 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\perfect.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            rect.Fill = brsh;
            Canvas.SetLeft(rect, x - (rect.Width/2));
            Canvas.SetTop(rect, y + 60);
            Models.HUDTemplate _status = new Models.HUDTemplate(rect, 0.5);
            ScaleTransform scaly = new ScaleTransform(0.5, 0.5, 187.5, 56.5);
            rect.RenderTransform = scaly;
            status.Add(_status);
            canvas.Children.Add(rect);
        }

        public void Great(double x, double y, Canvas canvas, double width, double height)
        {
            Rectangle rect = new Rectangle { Width = 375, Height = 113 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Great.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            rect.Fill = brsh;
            Canvas.SetLeft(rect, x - (rect.Width / 2));
            Canvas.SetTop(rect, y + 60);
            Models.HUDTemplate _status = new Models.HUDTemplate(rect, 0.5);
            ScaleTransform scaly = new ScaleTransform(0.5, 0.5, 187.5, 56.5);
            rect.RenderTransform = scaly;
            status.Add(_status);
            canvas.Children.Add(rect);
        }

        public void Missing(double x, double y, Canvas canvas, double width, double height)
        {
            Rectangle _rect = new Rectangle { Width = 80, Height = 80 };
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\Miss.png", UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            _rect.Fill = brsh;
            Canvas.SetLeft(_rect, x);
            Canvas.SetTop(_rect, y);
            Models.HUDTemplate _status = new Models.HUDTemplate(_rect, 0.5);
            ScaleTransform scaly = new ScaleTransform(0.5, 0.5, 40, 40);
            _rect.RenderTransform = scaly;
            canvas.Children.Add(_rect);
            miss.Add(_status);
        }

        public int HealthBarController(int data)
        {
            if (bar.Bar <= 5000 && bar.Bar > 0)
            {
                if (data == 0)
                {
                    bar.Bar -= 500;
                    if (bar.Bar < 0)
                    {
                        bar.Bar = 0;
                    }
                    double x = (bar.Bar * ((double)560 / (double)5000)) * ((double)1 / (double)560);
                    ScaleTransform scaly = new ScaleTransform(x, 1, 0, 0);
                    barImage.RenderTransform = scaly;
                }
                else if (data == 1)
                {
                    bar.Bar += 100;
                    if (bar.Bar > 5000)
                    {
                        bar.Bar = 5000;
                    }
                    double x = (bar.Bar * ((double)560 / (double)5000)) * ((double)1 / (double)560);
                    ScaleTransform scaly = new ScaleTransform(x, 1, 0, 0);
                    barImage.RenderTransform = scaly;
                }
                else if (data == 2)
                {
                    bar.Bar += 300;
                    if (bar.Bar > 5000)
                    {
                        bar.Bar = 5000;
                    }
                    double x = (bar.Bar * ((double)560 / (double)5000)) * ((double)1 / (double)560);
                    ScaleTransform scaly = new ScaleTransform(x, 1, 0, 0);
                    barImage.RenderTransform = scaly;
                }
            }
            return (int) bar.Bar / 50;
        }

    }
}
