﻿using System;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ARC.Helpers
{
    public delegate Vector2 SkeletonPointMap(SkeletonPoint point);

    class SkeletonDrawer
    {
        private KinectSensor sensor;
        private readonly SkeletonPointMap mapMethod;

        public SkeletonDrawer(KinectSensor _sensor)
        {
            this.sensor = _sensor;
            this.mapMethod = SkeletonToColorMap;
        }

        public Vector2 Draw(JointCollection joints, JointType startJoint)
        {
            
            Vector2 start = this.mapMethod(joints[startJoint].Position);
            return start;
        }

        private Vector2 SkeletonToColorMap(SkeletonPoint point)
        {
            if ((null != sensor) && (null != sensor.ColorStream))
            {
                var colorPt = sensor.CoordinateMapper.MapSkeletonPointToColorPoint(point, sensor.ColorStream.Format);
                return new Vector2(colorPt.X, colorPt.Y);
            }

            return Vector2.Zero;
        }

    }
}
