﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Navigation;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using WPFSoundVisualizationLib;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using System.Windows.Media.Animation;
using Microsoft.Xna.Framework;
using Coding4Fun.Kinect.Wpf.Controls;


namespace ARC
{
    public partial class MainWindow : Window
    {
        KinectSensor sensor = KinectSensor.KinectSensors[0];
        private List<Models.Music> iniMusik = new List<Models.Music>();
        byte[] pixelData;
        private KinectSensorChooser sensorChooser;
        private int objectCounter;
        private Skeleton[] skeletons = null;
        Skeleton _skeleton;
        private Helpers.ObjectHandler obj;
        private bool state;
        private double newTimer;
        private int statusLatihan;
        private List<Models.Object> rect = new List<Models.Object>();
        private List<Models.SkeletonPoint> skelp = new List<Models.SkeletonPoint>();
        private Helpers.HUD_Layer hud;
        private Helpers.SkeletonDrawer skDraw;
        private Models.Object _rect;
        int level, score, gameState, menuCountDown;
        public double windowHeight { get; set; }
        public double windowWidth { get; set; }
        bool initiating;
        MainMenu menuUtama;
        bool intersectStatus;
        bool tiraiStatus;
        NAudioEngine _engine;
        DigitalClock waktu;
        int tempat;
        RotateTransform rt;

        //Menu
        private EndGame end;

        //Latihan
        private Models.Object satu;
        private System.Windows.Shapes.Rectangle dua;
        private System.Windows.Shapes.Rectangle tiga;
        private System.Windows.Shapes.Rectangle empat;
        private System.Windows.Rect rectSatu;
        private System.Windows.Rect rectDua;

        
        public MainWindow()
        {
            gameState = 1;
            tempat = 0;
            InitializeComponent();

            nilai.Visibility = Visibility.Hidden;
            bg_atas.Visibility = Visibility.Hidden;
            gelembung.Visibility = Visibility.Hidden;
            label2.Visibility = Visibility.Hidden;
            label1.Visibility = Visibility.Hidden;
            textBlock1.Visibility = Visibility.Hidden;
            bar.Visibility = Visibility.Hidden;
            image1.Visibility = Visibility.Hidden;
            clockDisplay.Visibility = Visibility.Hidden;
            initiating = false;

            this.state = false;
            Loaded += OnLoaded;
            this.Unloaded += new RoutedEventHandler(MainWindow_Unloaded);

            Mouse.OverrideCursor = Cursors.None;
            sensor.ColorStream.Enable();
            sensor.SkeletonStream.Enable();
            Canvas.SetLeft(tiraiKiri, 0);
            Canvas.SetLeft(tiraiKanan, windowWidth / 2);
        }

        private void pergeseranTempat()
        {
            if (tempat == 0)
            {
                Canvas.SetLeft(tempat4, Canvas.GetLeft(tempat4) - 80);
                if (Canvas.GetLeft(tempat4) <= 0)
                {
                    Canvas.SetLeft(tempat4, 0);
                    tempat++;
                }
            }
            else if (tempat == 1)
            {
                Canvas.SetLeft(tempat3, Canvas.GetLeft(tempat3) - 80);
                if (Canvas.GetLeft(tempat3) <= 0)
                {
                    Canvas.SetLeft(tempat3, 0);
                    tempat++;
                }
            }
            else if (tempat == 2)
            {
                Canvas.SetLeft(tempat2, Canvas.GetLeft(tempat2) - 80);
                if (Canvas.GetLeft(tempat2) <= 0)
                {
                    Canvas.SetLeft(tempat2, 0);
                    tempat++;
                }
            }
            else if (tempat == 3)
            {
                Canvas.SetLeft(tempat1, Canvas.GetLeft(tempat1) - 80);
                if (Canvas.GetLeft(tempat1) <= 0)
                {
                    Canvas.SetLeft(tempat1, 0);
                    tempat++;
                }
            }
            else if (tempat == 4)
            {
                Canvas.SetLeft(logo, Canvas.GetLeft(logo) + 80);
                if (Canvas.GetLeft(logo) > 100)
                {
                    Canvas.SetLeft(logo, 100);
                    tempat++;
                }
            }
        }

        private void GameControl() 
        {
            if (gameState == 1)
            {
                //Menu
                this.initiateMenu();
                this.pergeseranTempat();
                if (initiating == true)
                {
                    if (menuUtama.menu == 0 && menuUtama != null)
                    {

                        this.CheckButton(menuUtama.butt[0], hud.bound[0], 0);
                        this.CheckButton(menuUtama.butt[1], hud.bound[0], 1);
                        this.CheckButton(menuUtama.butt[2], hud.bound[0], 2);
                        this.CheckButton(menuUtama.butt[3], hud.bound[0], 3);
                        if (menuUtama.NextStep(hud.scale, hud.bound[8]) == 1)
                        {
                            Canvas.SetLeft(tempat1, windowWidth);
                            Canvas.SetLeft(tempat2, windowWidth);
                            Canvas.SetLeft(tempat3, windowWidth);
                            Canvas.SetLeft(tempat4, windowWidth);
                            Canvas.SetLeft(logo, 0 - logo.Width);
                            bg_atas.Visibility = Visibility.Visible;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            for (int i = 0; i < 4; i++)
                            {
                                int j = i + 4;
                                canvas.Children.Add(hud.bound[j]);
                            }
                        }
                        else if (menuUtama.NextStep(hud.scale, hud.bound[8]) == 2)
                        {
                            Canvas.SetLeft(tempat1, windowWidth);
                            Canvas.SetLeft(tempat2, windowWidth);
                            Canvas.SetLeft(tempat3, windowWidth);
                            Canvas.SetLeft(tempat4, windowWidth);
                            Canvas.SetLeft(logo, 0 - logo.Width);
                            bg_atas.Visibility = Visibility.Visible;
                            rt.BeginAnimation(RotateTransform.AngleProperty, null);
                            gelembung.Visibility = Visibility.Hidden;
                            gelembung2.Visibility = Visibility.Hidden;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            for (int i = 0; i < 4; i++)
                            {
                                int j = i + 4;
                                canvas.Children.Add(hud.bound[j]);
                            }
                            this.level = 1;
                            menuUtama.menu = 2;
                        }
                        else if (menuUtama.NextStep(hud.scale, hud.bound[8]) == 3)
                        {
                            Canvas.SetLeft(tempat1, windowWidth);
                            Canvas.SetLeft(tempat2, windowWidth);
                            Canvas.SetLeft(tempat3, windowWidth);
                            Canvas.SetLeft(tempat4, windowWidth);
                            Canvas.SetLeft(logo, 0 - logo.Width);
                            bg_atas.Visibility = Visibility.Visible;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            for (int i = 0; i <= 4; i++)
                            {
                                int j = i + 4;
                                canvas.Children.Add(hud.bound[j]);
                            }
                            menuUtama.menu = 4;
                        }
                    }
                    else if (menuUtama.menu == 1)
                    {
                        this.CheckButton(menuUtama.butt[4], hud.bound[0], 4);
                        this.CheckButton(menuUtama.butt[5], hud.bound[0], 5);
                        this.CheckButton(menuUtama.butt[6], hud.bound[0], 6);
                        this.CheckButton(menuUtama.butt[7], hud.bound[0], 7);
                        if (menuUtama.NextStep(hud.scale) == 1)
                        {
                            rt.BeginAnimation(RotateTransform.AngleProperty, null);
                            gelembung.Visibility = Visibility.Hidden;
                            gelembung2.Visibility = Visibility.Hidden;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            canvas.Children.Add(hud.bound[8]);
                            for (int i = 0; i < 4; i++)
                            {
                                int j = i + 4;
                                canvas.Children.Add(hud.bound[j]);
                            }
                        }
                        else if (menuUtama.NextStep(hud.scale) == 2)
                        {
                            this.CheckButton(1);
                            menuUtama.changeMusicState = true;
                            menuUtama.SongChooser--;
                            if (menuUtama.SongChooser < 0)
                            {
                                menuUtama.SongChooser = 0;
                            }
                            if (menuUtama.changeMusicState == true)
                            {
                                menuUtama.changeMusic();
                                menuUtama.changeMusicState = false;
                            }
                        }
                        else if (menuUtama.NextStep(hud.scale) == 3)
                        {
                            this.CheckButton(2);
                            menuUtama.changeMusicState = true;
                            menuUtama.SongChooser++;
                            if (menuUtama.SongChooser > menuUtama.sv.Count - 1)
                            {
                                menuUtama.SongChooser = menuUtama.sv.Count - 1;
                            }
                            if (menuUtama.changeMusicState == true)
                            {
                                menuUtama.changeMusic();
                                menuUtama.changeMusicState = true;
                            }
                        }
                        else if (menuUtama.NextStep(hud.scale) == 4)
                        {
                            menuUtama.menu = 0;
                            canvas.Children.Clear();
                            initiating = false;
                            this.tempat = 0;
                        }
                    }
                    else if (menuUtama.menu == 3)
                    {
                        this.CheckButton(menuUtama.butt[8], hud.bound[0], 8);
                        this.CheckButton(menuUtama.butt[9], hud.bound[0], 9);
                        if (menuUtama.NextStep(hud.scale, hud.bound[8]) == 1)
                        {
                            this.level = 1;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            menuUtama.menu = 2;
                        }
                        else if (menuUtama.NextStep(hud.scale, hud.bound[8]) == 2)
                        {
                            this.level = 2;
                            for (int i = 0; i < 4; i++)
                            {
                                this.CheckButton(i);
                            }
                            menuUtama.menu = 2;
                        }
                    }
                    else if (menuUtama.menu == 2)
                    {
                        if (tiraiStatus == false)
                        {
                            menuUtama.addInfo();
                            menuUtama.badan();
                            tiraiKiri.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKiri), 0 - ((tiraiKiri.Width * 3) / 4), new Duration(TimeSpan.FromSeconds(0.5))));
                            tiraiKanan.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKanan), (tiraiKanan.Width * 3) / 4, new Duration(TimeSpan.FromSeconds(0.5))));
                            tiraiStatus = true;
                        }
                    }
                    else if (menuUtama.menu == 4)
                    {
                        this.CheckButton(menuUtama.butt[4], hud.bound[0], 4);
                        if (menuUtama.NextStep(hud.scale) == 1)
                        {
                            menuUtama.menu = 0;
                            canvas.Children.Clear();
                            initiating = false;
                            this.tempat = 0;
                        }
                    }
                }
            }
            else if (gameState == 2)
            {
                //Game
                this.initiateGame();
                foreach (Models.Object _rect in rect.ToList())
                {
                    List<Rect> masterRect = new List<Rect>();
                    for (int i = 0; i < 4; i++)
                    {
                        //0 is kanan ; 1 is kiri ; 2 is Kkanan ; 3 is kKiri
                        Rect rect1 = hud.boundingBox(hud.bound[i]);
                        masterRect.Add(rect1);
                    }
                    Rect rect2 = hud.boundingBox(_rect.getRect);
                    if (level == 1)
                    {
                        //Tangan Kanan
                        if (_rect.Intersect != 0)
                        {
                            if (masterRect[0].IntersectsWith(rect2) || masterRect[1].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                            //Tangan Kiri
                            else if (masterRect[1].IntersectsWith(rect2) || masterRect[0].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                        }
                    }
                    else if (level == 2)
                    {
                        if (_rect.Intersect != 0)
                        {
                            //Tangan Kanan
                            if (masterRect[0].IntersectsWith(rect2) || masterRect[1].IntersectsWith(rect2) || masterRect[2].IntersectsWith(rect2) || masterRect[3].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                            //Tangan Kiri
                            else if (masterRect[0].IntersectsWith(rect2) || masterRect[1].IntersectsWith(rect2) || masterRect[2].IntersectsWith(rect2) || masterRect[3].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                            //Kaki Kanan
                            else if (masterRect[0].IntersectsWith(rect2) || masterRect[1].IntersectsWith(rect2) || masterRect[2].IntersectsWith(rect2) || masterRect[3].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                            //Kaki Kiri
                            else if (masterRect[0].IntersectsWith(rect2) || masterRect[1].IntersectsWith(rect2) || masterRect[2].IntersectsWith(rect2) || masterRect[3].IntersectsWith(rect2))
                            {
                                intersectStatus = false;
                                this.IntersectionMethod(_rect.Intersect, _rect);
                            }
                        }
                    }
                }
                if (menuUtama.latihan)
                {
                    this.methodLatihan();
                }
                this.GameLogic();
                this.endGame();
            }
            else if (gameState == 3)
            {
                this.initiateEnd();
                this.CheckButton(end._button[0], hud.bound[8], 0);
                this.CheckButton(end._button[1], hud.bound[8], 1);
                if (end.NextStep(hud.scale) == 2)
                {
                    canvas.Children.Clear();
                    initiating = false;
                    gameState = 1;
                    tempat = 0;
                }
                else if (end.NextStep(hud.scale) == 1)
                {
                    canvas.Children.Clear();
                    initiating = false;
                    gameState = 2;
                }
            }
        }

        #region Latihan
        private void methodLatihan()
        {
            if (statusLatihan == 0)
            {
                if (clockDisplay.Time.TotalMilliseconds >= 6250 && clockDisplay.Time.TotalMilliseconds <= 6300)
                {
                    NAudioEngine.Instance.Stop();
                    //Tahap 1
                    //Objek
                    satu = new Models.Object(obj.drawRect(objectCounter, this.level), newTimer, obj.getControl(objectCounter, level));
                    Canvas.SetLeft(satu.getRect,hud.Template[3].PosX);
                    Canvas.SetTop(satu.getRect,hud.Template[3].PosY);
                    satu.getRect.Opacity = 1;
                    canvas.Children.Add(satu.getRect);
                    rectSatu = new Rect(Canvas.GetLeft(satu.getRect), Canvas.GetTop(satu.getRect), satu.getRect.Width, satu.getRect.Height);

                    //Panah
                    dua = new System.Windows.Shapes.Rectangle { Width = 52, Height = 69 };
                    ImageBrush brsh = new ImageBrush();
                    BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\panah1.png", UriKind.Relative));
                    brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                    dua.Fill = brsh;
                    Canvas.SetLeft(dua, hud.Template[3].PosX - 52);
                    Canvas.SetTop(dua, hud.Template[3].PosY + 69);
                    canvas.Children.Add(dua);
                   
                    //Instruksi1
                    tiga = new System.Windows.Shapes.Rectangle { Width = 200, Height = 75 };
                    ImageBrush brsh2 = new ImageBrush();
                    BitmapImage bmi2 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\instruksi1.png", UriKind.Relative));
                    brsh2.ImageSource = bmi2; brsh2.Opacity = 1.0;
                    tiga.Fill = brsh2;
                    Canvas.SetLeft(tiga, Canvas.GetLeft(satu.getRect) - 100 - 52);
                    Canvas.SetTop(tiga, Canvas.GetTop(satu.getRect) + 75 + 69);
                    canvas.Children.Add(tiga);

                    statusLatihan = 1;
                }
            }
            else if (statusLatihan == 1)
            {
                //Tahap 2
                rectDua = hud.boundingBox(hud.bound[0]);
                if (rectSatu.IntersectsWith(rectDua))
                {
                    NAudioEngine.Instance.Play();
                    hud.Perfect(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    canvas.Children.Remove(satu.getRect);
                    canvas.Children.Remove(dua);
                    canvas.Children.Remove(tiga);
                    int x = this.hud.HealthBarController(2);
                    label1.Content = x.ToString();
                    score += 500;
                    label2.Content = score;
                    statusLatihan = 2;
                }
            }
            else if (statusLatihan == 2)
            { 
                //Tahap 3
                if (clockDisplay.Time.TotalMilliseconds >= 10950 && clockDisplay.Time.TotalMilliseconds <= 11020)
                {
                    NAudioEngine.Instance.Stop();
                    //Tahap 3
                    //Objek
                    satu = new Models.Object(obj.drawRect(objectCounter, this.level), newTimer, obj.getControl(objectCounter, level));
                    Canvas.SetLeft(satu.getRect, hud.Template[3].PosX);
                    Canvas.SetTop(satu.getRect, hud.Template[3].PosY);
                    satu.getRect.Opacity = 1;
                    canvas.Children.Add(satu.getRect);
                    rectSatu = new Rect(Canvas.GetLeft(satu.getRect), Canvas.GetTop(satu.getRect), satu.getRect.Width, satu.getRect.Height);

                    //Panah
                    dua = new System.Windows.Shapes.Rectangle { Width = 52, Height = 69 };
                    ImageBrush brsh = new ImageBrush();
                    BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\panah1.png", UriKind.Relative));
                    brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                    dua.Fill = brsh;
                    Canvas.SetLeft(dua, hud.Template[3].PosX - 52);
                    Canvas.SetTop(dua, hud.Template[3].PosY + 69);
                    canvas.Children.Add(dua);

                    //Instruksi1
                    tiga = new System.Windows.Shapes.Rectangle { Width = 200, Height = 75 };
                    ImageBrush brsh2 = new ImageBrush();
                    BitmapImage bmi2 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\instruksi1.png", UriKind.Relative));
                    brsh2.ImageSource = bmi2; brsh2.Opacity = 1.0;
                    tiga.Fill = brsh2;
                    Canvas.SetLeft(tiga, Canvas.GetLeft(satu.getRect) - 100 - 52);
                    Canvas.SetTop(tiga, Canvas.GetTop(satu.getRect) + 75 + 69);
                    canvas.Children.Add(tiga);

                    statusLatihan = 3;
                }
            }
            else if (statusLatihan == 3)
            {
                //Tahap 4
                rectDua = hud.boundingBox(hud.bound[0]);
                if (rectSatu.IntersectsWith(rectDua))
                {
                    NAudioEngine.Instance.Play();
                    hud.Perfect(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    canvas.Children.Remove(satu.getRect);
                    canvas.Children.Remove(dua);
                    canvas.Children.Remove(tiga);
                    int x = this.hud.HealthBarController(2);
                    label1.Content = x.ToString();
                    score += 500;
                    label2.Content = score;
                    statusLatihan = 4;
                }
            }
            else if (statusLatihan == 4)
            {
                //Tahap 5
                if (clockDisplay.Time.TotalMilliseconds >= 16315 && clockDisplay.Time.TotalMilliseconds <= 16365)
                {
                    NAudioEngine.Instance.Stop();
                    //Tahap 5
                    //Objek
                    satu = new Models.Object(obj.drawRect(objectCounter, this.level), newTimer, obj.getControl(objectCounter, level));
                    Canvas.SetLeft(satu.getRect, hud.Template[2].PosX);
                    Canvas.SetTop(satu.getRect, hud.Template[2].PosY);
                    satu.getRect.Opacity = 1;
                    canvas.Children.Add(satu.getRect);
                    rectSatu = new Rect(Canvas.GetLeft(satu.getRect), Canvas.GetTop(satu.getRect), satu.getRect.Width, satu.getRect.Height);

                    //Panah
                    dua = new System.Windows.Shapes.Rectangle { Width = 52, Height = 69 };
                    ImageBrush brsh = new ImageBrush();
                    BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\panah2.png", UriKind.Relative));
                    brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                    dua.Fill = brsh;
                    Canvas.SetLeft(dua, 120);
                    Canvas.SetTop(dua, 65); 
                    canvas.Children.Add(dua);

                    //Instruksi3
                    tiga = new System.Windows.Shapes.Rectangle { Width = 200, Height = 75 };
                    ImageBrush brsh2 = new ImageBrush();
                    BitmapImage bmi2 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\instruksi2.png", UriKind.Relative));
                    brsh2.ImageSource = bmi2; brsh2.Opacity = 1.0;
                    tiga.Fill = brsh2;
                    Canvas.SetLeft(tiga, 150 - 100);
                    Canvas.SetTop(tiga, 65 + 75);
                    canvas.Children.Add(tiga);

                    statusLatihan = 5;
                }
            }
            else if (statusLatihan == 5)
            {
                //Tahap 6
                rectDua = hud.boundingBox(hud.bound[1]);
                if (rectSatu.IntersectsWith(rectDua))
                {
                    NAudioEngine.Instance.Play();
                    hud.Perfect(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    canvas.Children.Remove(satu.getRect);
                    canvas.Children.Remove(dua);
                    canvas.Children.Remove(tiga);
                    int x = this.hud.HealthBarController(2);
                    label1.Content = x.ToString();
                    score += 500;
                    label2.Content = score;
                    statusLatihan = 6;
                }
            }
            else if (statusLatihan == 6)
            {
                //Tahap 7
                if (clockDisplay.Time.TotalMilliseconds >= 21100 && clockDisplay.Time.TotalMilliseconds <= 21217)
                {
                    NAudioEngine.Instance.Stop();
                    //Tahap 7
                    //Objek
                    satu = new Models.Object(obj.drawRect(objectCounter, this.level), newTimer, obj.getControl(objectCounter, level));
                    Canvas.SetLeft(satu.getRect, hud.Template[3].PosX);
                    Canvas.SetTop(satu.getRect, hud.Template[3].PosY);
                    satu.getRect.Opacity = 1;
                    canvas.Children.Add(satu.getRect);
                    rectSatu = new Rect(Canvas.GetLeft(satu.getRect), Canvas.GetTop(satu.getRect), satu.getRect.Width, satu.getRect.Height);

                    //Panah
                    dua = new System.Windows.Shapes.Rectangle { Width = 52, Height = 69 };
                    ImageBrush brsh = new ImageBrush();
                    BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\panah2.png", UriKind.Relative));
                    brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                    dua.Fill = brsh;
                    Canvas.SetLeft(dua, windowWidth - 120);
                    Canvas.SetTop(dua, 65);
                    canvas.Children.Add(dua);

                    //Instruksi
                    tiga = new System.Windows.Shapes.Rectangle { Width = 200, Height = 75 };
                    ImageBrush brsh2 = new ImageBrush();
                    BitmapImage bmi2 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\instruksi3.png", UriKind.Relative));
                    brsh2.ImageSource = bmi2; brsh2.Opacity = 1.0;
                    tiga.Fill = brsh2;
                    Canvas.SetLeft(tiga, windowWidth - 150 - 100);
                    Canvas.SetTop(tiga, 65 + 75);
                    canvas.Children.Add(tiga);

                    //Instruksi
                    empat = new System.Windows.Shapes.Rectangle { Width = 200, Height = 75 };
                    ImageBrush brsh3 = new ImageBrush();
                    BitmapImage bmi3 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\instruksi4.png", UriKind.Relative));
                    brsh3.ImageSource = bmi3; brsh3.Opacity = 1.0;
                    empat.Fill = brsh3;
                    Canvas.SetLeft(empat, windowWidth / 2 - 100);
                    Canvas.SetTop(empat, windowHeight / 2);
                    canvas.Children.Add(empat);

                    statusLatihan = 7;
                }
            }
            else if (statusLatihan == 7)
            {
                //Tahap 8
                rectDua = hud.boundingBox(hud.bound[0]);
                if (rectSatu.IntersectsWith(rectDua))
                {
                    NAudioEngine.Instance.Play();
                    hud.Perfect(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    canvas.Children.Remove(satu.getRect);
                    canvas.Children.Remove(dua);
                    canvas.Children.Remove(tiga);
                    canvas.Children.Remove(empat);
                    int x = this.hud.HealthBarController(2);
                    label1.Content = x.ToString();
                    score += 500;
                    label2.Content = score;
                    statusLatihan = 9;
                }
            }
        }

        #endregion

        private void initiateEnd()
        {
            if (initiating == false)
            {
                label1.Visibility = Visibility.Hidden;
                textBlock1.Visibility = Visibility.Hidden;
                nilai.Visibility = Visibility.Hidden;
                label2.Visibility = Visibility.Hidden;
                bar.Visibility = Visibility.Hidden;
                image1.Visibility = Visibility.Hidden;
                initiating = true;
            }
        }

        private void initiateMenu()
        {
            if (initiating == false && tempat == 5)
            {
                gelembung.Visibility = Visibility.Visible;
                gelembung2.Visibility = Visibility.Visible;
                label1.Visibility = Visibility.Hidden;
                textBlock1.Visibility = Visibility.Hidden;
                nilai.Visibility = Visibility.Hidden;
                label2.Visibility = Visibility.Hidden;
                bar.Visibility = Visibility.Hidden;
                image1.Visibility = Visibility.Hidden;
                var da = new DoubleAnimation(0, 360, new Duration(TimeSpan.FromSeconds(10)));
                da.RepeatBehavior = RepeatBehavior.Forever;
                rt = new RotateTransform();
                gelembung.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                gelembung.RenderTransform = rt;
                gelembung2.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                gelembung2.RenderTransform = rt;
                rt.BeginAnimation(RotateTransform.AngleProperty,da );
                tiraiStatus = false;
                menuUtama = new MainMenu(canvas, windowWidth, windowHeight);
                canvas.Children.Add(hud.bound[8]);
                for (int i = 0; i < 4; i++)
                {
                    int j = i + 4;
                    canvas.Children.Add(hud.bound[j]);
                }
                initiating = true;
            }
            else if (initiating == false && tempat == 0)
            {
                bg_atas.Visibility = Visibility.Hidden;
                if (menuUtama != null)
                {
                    menuUtama.butt.RemoveRange(0, menuUtama.butt.Count);
                }
                this.skDraw = new Helpers.SkeletonDrawer(this.sensor);
                this.hud = new Helpers.HUD_Layer();
            }
        }

        private void initiateGame()
        {
            if (initiating == false)
            {
                intersectStatus = false;
                label1.Visibility = Visibility.Visible;
                textBlock1.Visibility = Visibility.Visible;
                nilai.Visibility = Visibility.Visible;
                label2.Visibility = Visibility.Visible;
                label2.Content = "0";
                bar.Visibility = Visibility.Visible;
                ScaleTransform scaly = new ScaleTransform(1, 1, 0, 0);
                bar.RenderTransform = scaly;
                image1.Visibility = Visibility.Visible;
                canvas.Children.Clear();
                canvas2.Children.Clear();
                canvas2.Children.Add(label1);
                canvas2.Children.Add(textBlock1);
                this.objectCounter = 0;
                this.newTimer = -1;
                this.state = false;
                menuCountDown = 0;
                Helpers.Music_Handler msc = new Helpers.Music_Handler(menuUtama.fileName);
                _engine = NAudioEngine.Instance;
                waktu = new DigitalClock();
                waktu.Time = TimeSpan.FromSeconds(_engine.ChannelLength);
                iniMusik = msc.GetListMusik;
                rect.RemoveRange(0, rect.Count);
                this.obj = new Helpers.ObjectHandler(iniMusik);
                this.skDraw = new Helpers.SkeletonDrawer(this.sensor);
                this.score = 0;
                this.hud = new Helpers.HUD_Layer(level, bar,menuUtama.kordinat,windowHeight,windowWidth);
                this.implementHUD(this.level);
                GameCore();
                this.addingskel();
                tiraiKiri.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKiri), 0 - ((tiraiKiri.Width * 3) / 4), new Duration(TimeSpan.FromSeconds(0.5))));
                tiraiKanan.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKanan), (tiraiKanan.Width * 3) / 4, new Duration(TimeSpan.FromSeconds(0.5))));
                menuUtama.menu = 0;
                if (menuUtama.latihan)
                {
                    statusLatihan = 0;
                }
                initiating = true;
            }
        }

        public void endGame()
        {
            //KALAH
            if (hud.bar.Bar <= 0 && gameState == 2)
            {
                this.menuCountDown = 0;
                if (menuCountDown == 0)
                {
                    initiating = false;
                    canvas.Children.Clear();
                    // End Game
                    NAudioEngine.Instance.Stop();
                    end = new EndGame(score,0, canvas, windowWidth, windowHeight);
                    menuCountDown = 1;
                    canvas.Children.Add(hud.bound[8]);
                    tiraiKiri.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKiri), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                    tiraiKanan.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKanan), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                }
                this.gameState = 3;
            }
            //MENANG
            else if (clockDisplay.Time.TotalSeconds == waktu.Time.TotalSeconds)
            {
                this.menuCountDown = 0;
                objectCounter = 0;
                this.state = true;
                if (menuCountDown == 0)
                {
                    initiating = false;
                    canvas.Children.Clear();
                    // End Game
                    NAudioEngine.Instance.Stop();
                    end = new EndGame(score, 1, canvas, windowWidth, windowHeight);
                    menuCountDown = 1;
                    canvas.Children.Add(hud.bound[8]);
                    tiraiKiri.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKiri), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                    tiraiKanan.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKanan), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                }
                this.gameState = 3;
            }
        }

        private void addingskel()
        {
            for (int i = 0; i < 4; i++)
            {
                canvas.Children.Add(hud.bound[i]);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement pnlClient = this.Content as FrameworkElement;
            if (pnlClient != null)
            {
                windowWidth = pnlClient.ActualWidth;
                windowHeight = pnlClient.ActualHeight;
                canvas.Width = windowWidth;
                canvas.Height = windowHeight;
            }
        }

        private void implementHUD(int _level)
        {
            if (_level == 1)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (this.hud.Template[i].Frame != null)
                    {
                        Canvas.SetLeft(hud.Template[i].Frame, hud.Template[i].PosX);
                        Canvas.SetTop(hud.Template[i].Frame, hud.Template[i].PosY);
                        canvas.Children.Add(this.hud.Template[i].Frame);
                    }
                }
            }
            else if (_level == 2)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (this.hud.Template[i].Frame != null)
                    {
                        Canvas.SetLeft(hud.Template[i].Frame, hud.Template[i].PosX);
                        Canvas.SetTop(hud.Template[i].Frame, hud.Template[i].PosY);
                        canvas.Children.Add(this.hud.Template[i].Frame);
                    }
                }
            }
        }

        void GameCore()
        {
            if (NAudioEngine.Instance.CanPlay)
            {
                NAudioEngine.Instance.Play();
                NAudioEngine soundEngine = NAudioEngine.Instance;
                soundEngine.PropertyChanged += NAudioEngine_PropertyChanged;
            }
        }

        #region NAudio Engine Events
        private void NAudioEngine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            NAudioEngine engine = NAudioEngine.Instance;
            switch (e.PropertyName)
            {
                case "ChannelPosition":
                    clockDisplay.Time = TimeSpan.FromSeconds(engine.ChannelPosition);
                    break;
                default:
                    // Do Nothing
                    break;
            }

        }
        #endregion

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.sensorChooser = new KinectSensorChooser();
            this.sensor.SkeletonFrameReady += SensorSkeletonFrameReady;
            sensor.ColorFrameReady += runtime_VideoFrameReady;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();
            
        }

        void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            sensor.Stop();
            this.sensorChooser.Stop();
        }

        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            this.GameControl();
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    if (this.skeletons == null)
                    {
                        this.skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    }
                    skeletonFrame.CopySkeletonDataTo(this.skeletons);
                    Skeleton skeleton = this.skeletons.Where(s => s.TrackingState == SkeletonTrackingState.Tracked).FirstOrDefault();
                    this._skeleton = skeleton;
                    if (skeleton != null)
                    {
                        // Obtain the left knee joint; if tracked, print its position
                        Joint j = skeleton.Joints[JointType.FootRight];
                        if (j.TrackingState == JointTrackingState.Tracked)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                SetEllipsePosition(hud.bound[i + 4], skDraw.Draw(skeleton.Joints, JointType.HandRight));
                                Canvas.SetTop(hud.bound[i + 4], Canvas.GetTop(hud.bound[i + 4]) - 25);
                            }
                            SetEllipsePosition(hud.bound[8], skDraw.Draw(skeleton.Joints, JointType.HandRight));
                            SetEllipsePosition(hud.bound[0], skDraw.Draw(skeleton.Joints, JointType.HandRight));
                            SetEllipsePosition(hud.bound[1], skDraw.Draw(skeleton.Joints, JointType.HandLeft));
                            SetEllipsePosition(hud.bound[2], skDraw.Draw(skeleton.Joints, JointType.FootRight));
                            SetEllipsePosition(hud.bound[3], skDraw.Draw(skeleton.Joints, JointType.FootLeft));
                            if (menuUtama != null)
                            {
                                if (menuUtama.menu == 2)
                                {
                                    if (menuUtama.countCalibrate <= 250)
                                    {
                                        menuUtama.countCalibrate++;
                                        menuUtama.counter++;
                                    }
                                    if (menuUtama.countCalibrate % 40 == 0)
                                    {
                                        menuUtama.counter = 0;
                                        menuUtama.badan();
                                    }
                                    if (menuUtama.counter == 25)
                                    {
                                        canvas.Children.Remove(menuUtama.badanKalibrasi);
                                    }
                                    if (menuUtama.countCalibrate % 50 == 0)
                                    {
                                        System.Media.SystemSounds.Asterisk.Play();
                                    }
                                    if (menuUtama.countCalibrate == 200)
                                    {
                                        menuUtama.setKordinat(SetPositionX(skDraw.Draw(skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(skeleton.Joints, JointType.ShoulderCenter)));
                                        menuUtama.setKordinat(SetPositionX(skDraw.Draw(skeleton.Joints, JointType.HipCenter)), SetPositionY(skDraw.Draw(skeleton.Joints, JointType.HipCenter)));
                                        menuUtama.setKordinat(SetPositionX(skDraw.Draw(skeleton.Joints, JointType.FootLeft)), SetPositionY(skDraw.Draw(skeleton.Joints, JointType.FootLeft)));
                                        menuUtama.setKordinat(SetPositionX(skDraw.Draw(skeleton.Joints, JointType.FootRight)), SetPositionY(skDraw.Draw(skeleton.Joints, JointType.FootRight)));
                                        tiraiKiri.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKiri), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                                        tiraiKanan.BeginAnimation(Canvas.LeftProperty, new DoubleAnimation(Canvas.GetLeft(tiraiKanan), 0, new Duration(TimeSpan.FromSeconds(0.5))));
                                    }
                                    if (menuUtama.countCalibrate == 250)
                                    {
                                        menuUtama.menu = 3;
                                        gameState = 2;
                                        initiating = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        
        private void CheckButton(HoverButton button, System.Windows.Shapes.Rectangle thumbStick, int i)
        {
            Rect rect1 = new Rect(Canvas.GetLeft(button), Canvas.GetTop(button), button.Width, button.Height);
            Rect rect2 = new Rect(Canvas.GetLeft(thumbStick), Canvas.GetTop(thumbStick), thumbStick.Width, thumbStick.Height);
            if (rect1.IntersectsWith(rect2))
            {
                button.Hovering();
                hud.scale[i] += 0.02;
                if (hud.scale[i] > 1)
                {
                    hud.scale[i] = 1;
                }
                ScaleTransform scaly = new ScaleTransform(hud.scale[i], 1, 0, 0);
                if (menuUtama.menu == 0 || gameState == 2)
                {
                    hud.bound[i + 4].RenderTransform = scaly;
                }
                else if (menuUtama.menu == 3)
                {
                    hud.bound[i - 4].RenderTransform = scaly;
                }
                else
                {
                    hud.bound[i].RenderTransform = scaly;
                }
            }
            else
            {
                button.Release();
                hud.scale[i] = 0;
                ScaleTransform scaly = new ScaleTransform(hud.scale[i], 1, 0, 0);
                if (menuUtama.menu == 0 || gameState == 2)
                {
                    hud.bound[i + 4].RenderTransform = scaly;
                }
                else if (menuUtama.menu == 3)
                {
                    hud.bound[i - 4].RenderTransform = scaly;
                }
                else
                {
                    hud.bound[i].RenderTransform = scaly;
                }
            }
        }

        private void CheckButton(int i)
        {
            if (menuUtama.menu != 1)
            {
                hud.scale[i] = 0;
            }
            else
            {
                hud.scale[i + 4] = 0;
            }
            ScaleTransform scaly = new ScaleTransform(hud.scale[i], 1, 0, 0);
            hud.bound[i + 4].RenderTransform = scaly;
        }

        private void IntersectionMethod(int data, Models.Object _rect)
        {
            if (intersectStatus == false)
            {
                // The two elements overlap
                if (data == 2)
                {
                    _rect.Intersect = 0;
                    score += 500;
                    //Hapus Perfect
                    int i = 0;
                    if (hud.status != null)
                    {
                        foreach (Models.HUDTemplate status in hud.status.ToList())
                        {
                            if (status != null)
                            {   
                                if (hud.status[i] != null)
                                {
                                    canvas.Children.Remove(hud.status[i].Status);
                                    hud.status.RemoveAt(i);
                                }
                            }
                            i++;
                        }
                    }
                    if (!menuUtama.latihan || statusLatihan > 8)
                    {
                        hud.Perfect(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    }
                    label2.Content = score;
                    _rect.getRect.BeginAnimation(Canvas.LeftProperty, null);
                    canvas.Children.Remove(_rect.getRect);
                    Canvas.SetLeft(_rect.getRect, -200);
                    int x = this.hud.HealthBarController(2);
                    label1.Content = x.ToString();
                    intersectStatus = true;
                }
                else if (data == 1)
                {
                    _rect.Intersect = -1;
                    score += 200;
                    //Hapus Great
                    int i = 0;
                    if (hud.status != null)
                    {
                        foreach (Models.HUDTemplate status in hud.status.ToList())
                        {
                            if (status != null)
                            {
                                if (hud.status[i] != null)
                                {
                                    canvas.Children.Remove(hud.status[i].Status);
                                    hud.status.RemoveAt(i);
                                }
                            }
                            i++;
                        }
                    }
                    if (!menuUtama.latihan || statusLatihan > 8)
                    {
                        hud.Great(SetPositionX(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), SetPositionY(skDraw.Draw(_skeleton.Joints, JointType.ShoulderCenter)), canvas, windowWidth, windowHeight);
                    }
                    label2.Content = score;
                    _rect.getRect.BeginAnimation(Canvas.LeftProperty, null);
                    canvas.Children.Remove(_rect.getRect);
                    Canvas.SetLeft(_rect.getRect, -200);
                    int x = this.hud.HealthBarController(1);
                    label1.Content = x.ToString();
                    intersectStatus = true;

                }
            }
        }

        private void GameLogic()
        {
            if (this.state == false)
            {
                newTimer = obj.getNextObjectTimer(objectCounter);
                this.state = true;
                _rect = new Models.Object(obj.drawRect(objectCounter, this.level), newTimer, obj.getControl(objectCounter, level));
                _rect.CoorY = obj.setY(this.hud, objectCounter, level);
                _rect.CoorX = obj.setX(level, objectCounter);
                rect.Add(_rect);
            }
            if (clockDisplay.Time.TotalMilliseconds >= (newTimer - 2000) && clockDisplay.Time.TotalMilliseconds <= (newTimer + 1900) && this.state == true)
            {
                Canvas.SetLeft(rect[objectCounter].getRect, rect[objectCounter].CoorX);
                Canvas.SetTop(rect[objectCounter].getRect, rect[objectCounter].CoorY);
                if (canvas.Children.Contains(rect[objectCounter].getRect))
                {
                    //Jika canvas sudah mengandung sesuatu
                }
                else
                {
                    canvas.Children.Add(rect[objectCounter].getRect);
                }
                this.objectControl(rect[objectCounter], objectCounter);
                if (objectCounter < iniMusik.Count && objectCounter != iniMusik.Count - 1)
                {
                    this.state = false;
                    objectCounter++;
                }
            }

            //Hapus Objek
            for (int i = 0; i < objectCounter; i++)
            {
                double x = Canvas.GetLeft(rect[i].getRect);
                double y = Canvas.GetTop(rect[i].getRect);
                int _posisiEdge = obj.getPosisiEdge(i, level);
                if(_posisiEdge > 2  && _posisiEdge < 6 || _posisiEdge == 7)
                {
                    if(x < hud.Template[_posisiEdge].PosX - 20 && x > hud.Template[_posisiEdge].PosX - 70 && rect[i].Intersect != 0)
                    {
                        if (rect[i] != null)
                        {
                            if (!menuUtama.latihan || statusLatihan > 8)
                            {
                                hud.Missing(hud.Template[_posisiEdge].PosX, hud.Template[_posisiEdge].PosY, canvas, windowWidth, windowHeight);
                            }
                            rect[i].getRect.BeginAnimation(Canvas.LeftProperty, null);
                            Canvas.SetLeft(_rect.getRect, -200);
                            canvas.Children.Remove(rect[i].getRect);
                            rect[i].Intersect = 0;
                            int nyawa = this.hud.HealthBarController(0);
                            label1.Content = nyawa.ToString();
                        }
                    }
                }
                else if(_posisiEdge < 3 || _posisiEdge == 6)
                {
                    if (x > hud.Template[_posisiEdge].PosX + 20 && x < hud.Template[_posisiEdge].PosX + 70 && rect[i].Intersect != 0)
                    {
                        if (rect[i] != null)
                        {
                            if (!menuUtama.latihan || statusLatihan > 8)
                            {
                                hud.Missing(hud.Template[_posisiEdge].PosX, hud.Template[_posisiEdge].PosY, canvas, windowWidth, windowHeight);
                            }
                            rect[i].getRect.BeginAnimation(Canvas.LeftProperty, null);
                            Canvas.SetLeft(rect[i].getRect, -200);
                            canvas.Children.Remove(rect[i].getRect);
                            rect[i].Intersect = 0;
                            int nyawa = this.hud.HealthBarController(0);
                            label1.Content = nyawa.ToString();
                        }
                    }
                }
            }

            //Intersect Area
            for (int i = 0; i < objectCounter; i++)
            {

                double x = Canvas.GetLeft(rect[i].getRect);
                int _posisiEdge = obj.getPosisiEdge(i, level);
                //Area Kanan
                if (_posisiEdge > 2 && _posisiEdge < 6 || _posisiEdge == 7)
                {
                    if (x > hud.Template[_posisiEdge].PosX + 70 && x < hud.Template[_posisiEdge].PosX + 80)
                    {
                        if (rect[i] != null)
                        {
                            rect[i].Intersect = 1;
                        }
                    }
                    else if (x > hud.Template[_posisiEdge].PosX + 0 && x < hud.Template[_posisiEdge].PosX + 15 && rect[i].Intersect != -1)
                    {
                        if (rect[i] != null)
                        {
                            rect[i].Intersect = 2;
                        }
                    }
                }
                //Area Kiri
                else if (_posisiEdge < 3 || _posisiEdge == 6)
                {
                    if (x < hud.Template[_posisiEdge].PosX - 70 && x > hud.Template[_posisiEdge].PosX - 80)
                    {
                        if (rect[i] != null)
                        {
                            rect[i].Intersect = 1;
                            
                        }
                    }
                    else if (x < hud.Template[_posisiEdge].PosX - 0 && x > hud.Template[_posisiEdge].PosX - 15 && rect[i].Intersect != -1)
                    {
                        if (rect[i] != null)
                        {
                            rect[i].Intersect = 2;
                        }
                    }
                }
            }
            this.AnimateStatus();
            this.AnimateMissStatus();
        }

        private void objectControl(Models.Object _rct, int _counter)
        {
            if (_rct != null)
            {
                //Position Animation
                int _posisiEdge = obj.getPosisiEdge(_counter, level);
                var da = new DoubleAnimation(0, hud.Template[_posisiEdge].PosX, new Duration(TimeSpan.FromSeconds(2)));
                if (iniMusik[_counter].Kontrol == 2 || iniMusik[_counter].Kontrol == 4)
                {
                    da = new DoubleAnimation(Canvas.GetLeft(_rct.getRect), hud.Template[_posisiEdge].PosX + 50, new Duration(TimeSpan.FromSeconds(2.1)));
                }
                else if (iniMusik[_counter].Kontrol == 1 || iniMusik[_counter].Kontrol == 3)
                {
                    da = new DoubleAnimation(Canvas.GetLeft(_rct.getRect), hud.Template[_posisiEdge].PosX - 50, new Duration(TimeSpan.FromSeconds(2.1)));
                }
                _rct.getRect.BeginAnimation(Canvas.LeftProperty, da);

                //Opacity Animation
                da = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromSeconds(1.8)));
                _rct.getRect.BeginAnimation(OpacityProperty, da);
            }
        }

        private void AnimateStatus()
        {
            //
            int i = 0;
            if (hud.status != null)
            {
                foreach (Models.HUDTemplate status in hud.status.ToList())
                {
                    if (status != null)
                    {
                        status.ScaleSize += 0.05;
                        ScaleTransform scaly = new ScaleTransform(status.ScaleSize, status.ScaleSize, 187.5, 56.5);
                        status.Status.RenderTransform = scaly;
                        if (status.ScaleSize > 1.0)
                        {
                            canvas.Children.Remove(status.Status);
                            if (i < hud.status.Count)
                            {
                                if (hud.status[i] != null)
                                {
                                    hud.status.RemoveAt(i);
                                }
                            }
                        }
                    }
                    i++;
                }
            }
        }

        private void AnimateMissStatus() 
        {
            int i = 0;
            if (hud.miss != null)
            {
                foreach (Models.HUDTemplate status in hud.miss.ToList())
                {
                    if (status != null)
                    {
                        status.ScaleSize += 0.1;
                        ScaleTransform scaly = new ScaleTransform(status.ScaleSize, status.ScaleSize, 40, 40);
                        status.Status.RenderTransform = scaly;
                        if (status.ScaleSize > 1.0)
                        {
                            canvas.Children.Remove(status.Status);
                            if (i < hud.miss.Count)
                            {
                                if (hud.miss[i] != null)
                                {
                                    hud.miss.RemoveAt(i);
                                }
                            }
                        }
                    }
                    i++;
                }
            }
        }

        private void SetEllipsePosition(System.Windows.Shapes.Rectangle rct, Vector2 vector)
        {
            double x = vector.X / (640 / windowWidth);
            double y = vector.Y / (480 / windowHeight);
            Canvas.SetLeft(rct, x-15);
            Canvas.SetTop(rct, y-15);
            rct.Visibility = Visibility.Visible;
        }

        private double SetPositionX(Vector2 vector)
        {
            return vector.X / (640 / windowWidth);
        }

        private double SetPositionY(Vector2 vector)
        {
            return vector.Y / (480 / windowHeight);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            //Tempat Masalah Sensor nanti
        }

        void runtime_VideoFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            bool receivedData = false;

            using (ColorImageFrame CFrame = e.OpenColorImageFrame())
            {
                if (CFrame == null)
                {
                }
                else
                {
                    pixelData = new byte[CFrame.PixelDataLength];
                    CFrame.CopyPixelDataTo(pixelData);
                    receivedData = true;
                }
            }

            if (receivedData)
            {
                sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                BitmapSource source = BitmapSource.Create(640, 480, 96, 96, PixelFormats.Bgr32, null, pixelData, 640 * 4);

                videoImage.Source = source;
            }
        }
    }
}
