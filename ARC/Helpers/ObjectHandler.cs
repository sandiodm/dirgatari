﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using WPFSoundVisualizationLib;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace ARC.Helpers
{
    class ObjectHandler
    {
        private List<Models.Music> musik = new List<Models.Music>();
        private Rectangle rect;
        private String dir;
        private Random rnd = new Random();

        public ObjectHandler(List<Models.Music> _musik)
        {
            this.musik = _musik;
        }

        public Rectangle drawRect(int objectCounter, int level) 
        {
            rect = new Rectangle{ Width = 80, Height = 80};
            ImageBrush brsh = new ImageBrush();
            BitmapImage bmi = new BitmapImage(new Uri(getKontrol1Texture(objectCounter, level), UriKind.Relative));
            brsh.ImageSource = bmi; brsh.Opacity = 1.0;
            rect.Fill = brsh;
            ScaleTransform scaly = new ScaleTransform(1,1, 40,40);
            rect.RenderTransform = scaly;
            rect.Opacity = 0.5;
            return rect;
        }

        public double setY(Helpers.HUD_Layer hud, int id, int level)
        {
            if (level == 1) 
            { 
                if(musik[id].Posisi == 6)
                {
                    musik[id].Posisi = 2;
                }
                else if (musik[id].Posisi == 7)
                {
                    musik[id].Posisi = 4;
                }
                if (musik[id].Kontrol == 3)
                {
                    musik[id].Kontrol = 1;
                }
                else if (musik[id].Kontrol == 4)
                {
                    musik[id].Kontrol = 2;
                }
            }
            return hud.Template[musik[id].Posisi].PosY + 20;
        }

        public double setX(int level, int id)
        {
            if (level == 1)
            {
                if (musik[id].Posisi == 6)
                {
                    musik[id].Posisi = 0;
                }
                else if (musik[id].Posisi == 7)
                {
                    musik[id].Posisi = 3;
                }
            }
            if (musik[id].Kontrol == 1 || musik[id].Kontrol == 3)
            {
                return 1024;
            }
            else if (musik[id].Kontrol == 1 || musik[id].Kontrol == 4)
            {
                return 0;
            }
            return 0;
        }

        private string getKontrol1Texture(int id, int level)
        {
            this.dir = Directory.GetCurrentDirectory() + "\\Images\\Sprite";
            if (level == 1)
            {
                if (this.musik[id].Kontrol == 1)
                {
                    return dir + "\\Tangan_Kanan.png";
                }
                else if (this.musik[id].Kontrol == 2)
                {
                    return dir + "\\tangan_kiri.png";
                }
                else if (this.musik[id].Kontrol == 3)
                {
                    return dir + "\\kaki_kanan.png";
                }
                else if (this.musik[id].Kontrol == 4)
                {
                    return dir + "\\kaki_kiri.png";
                }
            }else if(level == 2)
            {
                if (this.musik[id].Kontrol == 1 )
                {
                    return dir + "\\Tangan_Kanan.png";
                }
                else if (this.musik[id].Kontrol == 2 )
                {
                    return dir + "\\tangan_kiri.png";
                }
                else if (this.musik[id].Kontrol == 3)
                {
                    return dir + "\\kaki_kanan.png";
                }
                else if (this.musik[id].Kontrol == 4)
                {
                    return dir + "\\kaki_kiri.png";
                }
            }
            return dir;
        }

        public double getNextObjectTimer(int objectCounter)
        {
            return musik[objectCounter].Time;
        }

        public int getPosisiEdge(int objectCounter, int level)
        {
            if (level == 1)
            {
                if (musik[objectCounter].Posisi == 6)
                {
                    musik[objectCounter].Posisi = 2;
                }
                else if (musik[objectCounter].Posisi == 7)
                {
                    musik[objectCounter].Posisi = 4;
                }
            }
            return musik[objectCounter].Posisi;
        }

        public int getControl(int objectCounter, int level)
        {
            if (level == 1) 
            {
                if (musik[objectCounter].Kontrol == 3)
                {
                    return 1;
                }
                else if (musik[objectCounter].Kontrol == 4)
                {
                    return 2;
                }
            }
            return musik[objectCounter].Kontrol;
        }

    }
}
