﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using WPFSoundVisualizationLib;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using System.Windows.Media.Animation;


namespace ARC
{
    public partial class MainWindow : Window
    {
        KinectSensor sensor = KinectSensor.KinectSensors[0];
        private List<Models.Music> iniMusik = new List<Models.Music>();
        byte[] pixelData;
        private KinectSensorChooser sensorChooser;
        private int objectCounter;
        private Skeleton[] skeletons = null;
        private Helpers.ObjectHandler obj;
        private bool state;
        private double newTimer;
        private List<Models.Object> rect = new List<Models.Object>();
        private Models.Object _rect;
        NAudioEngine engine;
        public double windowHeight { get; set; }
        public double windowWidth { get; set; }


        public MainWindow()
        {
            InitializeComponent();
            this.objectCounter = 0;
            this.newTimer = -1;
            clockDisplay.Visibility = Visibility.Hidden;

            this.state = false;
            Loaded += OnLoaded;
            this.Unloaded += new RoutedEventHandler(MainWindow_Unloaded);

            sensor.ColorStream.Enable();
            sensor.SkeletonStream.Enable();
            sensor.SkeletonStream.EnableTrackingInNearRange = false;
            
            Helpers.Music_Handler msc = new Helpers.Music_Handler("BanYa-Xenesis");
            iniMusik = msc.GetListMusik;
            this.obj = new Helpers.ObjectHandler(iniMusik);

            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement pnlClient = this.Content as FrameworkElement;
            if (pnlClient != null)
            {
                windowWidth = pnlClient.ActualWidth;
                windowHeight = pnlClient.ActualHeight;
            }
            //Canvas.SetLeft(shoulderCenter, windowWidth / 2);
            //Canvas.SetTop(shoulderCenter, (windowHeight * 1 / 3));
            //Canvas.SetLeft(hipCenter, windowWidth / 2);
            //Canvas.SetTop(hipCenter, (windowHeight * 2 / 3));
            //Canvas.SetLeft(elbowLeft, windowWidth - 100 );
            //Canvas.SetTop(elbowLeft, (windowHeight * 2 / 3));
            //Canvas.SetLeft(elbowRight, windowWidth * 3 / 4);
            //Canvas.SetTop(elbowRight, (windowHeight * 2 / 3));
            //Canvas.SetLeft(kneeLeft, windowWidth * 1 / 4);
            //Canvas.SetLeft(kneeRight, windowWidth * 3 / 4);
            //Canvas.SetLeft(footRight, windowWidth * 3 / 4);
            //Canvas.SetTop(footRight, windowHeight - 30);
            //Canvas.SetTop(kneeLeft, (windowHeight - (windowHeight * 3 / 4  )));
            //Canvas.SetTop(kneeRight, (windowHeight - (windowHeight * 3 / 4  )));
            
        }

        void GameCore()
        { 
            if (NAudioEngine.Instance.CanPlay)
            {
                NAudioEngine.Instance.Play();
                NAudioEngine soundEngine = NAudioEngine.Instance;
                soundEngine.PropertyChanged += NAudioEngine_PropertyChanged;
            }
            
        }

        #region NAudio Engine Events
        private void NAudioEngine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            NAudioEngine engine = NAudioEngine.Instance;
            switch (e.PropertyName)
            {
                case "ChannelPosition":
                    clockDisplay.Time = TimeSpan.FromSeconds(engine.ChannelPosition);
                    break;
                default:
                    // Do Nothing
                    break;
            }

        }
        #endregion

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            GameCore();
            this.sensorChooser = new KinectSensorChooser();
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensor.SkeletonFrameReady += SensorSkeletonFrameReady;
            sensor.ColorFrameReady += runtime_VideoFrameReady;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();
        }

        void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            sensor.Stop();
            this.sensorChooser.Stop();
        }

        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            #region skeleton Tracking
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    if (this.skeletons == null)
                    {
                        this.skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    }
                    skeletonFrame.CopySkeletonDataTo(this.skeletons);
                    Skeleton skeleton = this.skeletons.Where(s => s.TrackingState == SkeletonTrackingState.Tracked).FirstOrDefault();
                    if (skeleton != null)
                    {
                        // Obtain the left knee joint; if tracked, print its position
                        Joint j = skeleton.Joints[JointType.KneeLeft];

                        if (j.TrackingState == JointTrackingState.Tracked)
                        {
                            //SetEllipsePosition(head, skeleton.Joints[JointType.Head]);
                            //SetEllipsePosition(leftHand, skeleton.Joints[JointType.HandLeft]);
                            //SetEllipsePosition(rightHand, skeleton.Joints[JointType.HandRight]);
                            //SetEllipsePosition(elbowLeft, skeleton.Joints[JointType.ElbowLeft]);
                            //SetEllipsePosition(elbowRight, skeleton.Joints[JointType.ElbowRight]);
                            //SetEllipsePosition(shoulderLeft, skeleton.Joints[JointType.ShoulderLeft]);
                            //SetEllipsePosition(shoulderCenter, skeleton.Joints[JointType.ShoulderCenter]);
                            //SetEllipsePosition(shoulderRight, skeleton.Joints[JointType.ShoulderRight]);
                            //SetEllipsePosition(hipCenter, skeleton.Joints[JointType.HipCenter]);
                            //SetEllipsePosition(kneeLeft, skeleton.Joints[JointType.KneeLeft]);
                            //SetEllipsePosition(kneeRight, skeleton.Joints[JointType.KneeRight]);
                            //SetEllipsePosition(footLeft, skeleton.Joints[JointType.FootLeft]);
                            //SetEllipsePosition(footRight, skeleton.Joints[JointType.FootRight]);
                        }
                    }
                }
            }
            #endregion
            if (this.state == false)
            {
                newTimer = obj.getNextObjectTimer(objectCounter);
                this.state = true;
                _rect = new Models.Object(obj.drawRect(), newTimer);
                _rect.CoorX = obj.setX((int)Canvas.GetLeft(shoulderCenter), (int)Canvas.GetLeft(elbowRight), (int)Canvas.GetLeft(elbowLeft), (int)Canvas.GetLeft(kneeLeft), (int)Canvas.GetLeft(kneeRight), objectCounter);
                _rect.CoorY = obj.setY((int)Canvas.GetTop(shoulderCenter), (int)Canvas.GetTop(hipCenter), (int)Canvas.GetTop(kneeRight), (int)Canvas.GetTop(footRight), objectCounter);
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(obj.getKontrol1Texture(objectCounter), UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                _rect.getRect.Fill = brsh;
                rect.Add(_rect);
            }
            if (clockDisplay.Time.TotalMilliseconds >= (newTimer - 50) && clockDisplay.Time.TotalMilliseconds <= (newTimer + 50))
            {
                //obj.SpawnObject(skeleton.Joints[JointType.ShoulderCenter], skeleton.Joints[JointType.HipCenter], objectCounter);

                label1.Content = rect[objectCounter].CoorX.ToString();
                label2.Content = rect[objectCounter].CoorY.ToString();
                Canvas.SetLeft(rect[objectCounter].getRect, rect[objectCounter].CoorX);
                Canvas.SetTop(rect[objectCounter].getRect, rect[objectCounter].CoorY);
                canvas.Children.Add(rect[objectCounter].getRect);
                if (objectCounter < iniMusik.Count - 1)
                {
                    this.state = false;
                    objectCounter++;
                }
            }
            //Hapus Objek
            for (int i = 0; i < objectCounter; i++)
            {
                if (clockDisplay.Time.TotalMilliseconds >= (rect[i].getDropTime - 50) && clockDisplay.Time.TotalMilliseconds <= (rect[i].getDropTime + 50))
                {
                    if (rect[i] != null)
                    {
                        canvas.Children.Remove(rect[i].getRect);
                    }
                }
            }
        }

        private void SetEllipsePosition(Ellipse ellipse, Joint joint)
        {
            Microsoft.Kinect.SkeletonPoint vector = new Microsoft.Kinect.SkeletonPoint();
            vector.X = ScaleVector(1024, joint.Position.X);
            vector.Y = ScaleVector(768, -joint.Position.Y);
            vector.Z = ScaleVector(1600, joint.Position.Z);

            Joint updatedJoint = new Joint();
            updatedJoint = joint;
            updatedJoint.TrackingState = JointTrackingState.Tracked;
            updatedJoint.Position = vector;

            Canvas.SetLeft(ellipse, updatedJoint.Position.X);
            Canvas.SetTop(ellipse, updatedJoint.Position.Y);


        }

        private float ScaleVector(int length, float position)
        {
            float value = (((((float)length) / 1f) / 2f) * position) + (length / 2);
            if (value > length)
            {
                return (float)length;
            }
            if (value < 0f)
            {
                return 0f;
            }
            return value;
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            //Tempat Masalah Sensor nanti
        }

        void runtime_VideoFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            bool receivedData = false;

            using (ColorImageFrame CFrame = e.OpenColorImageFrame())
            {
                if (CFrame == null)
                {
                }
                else
                {
                    pixelData = new byte[CFrame.PixelDataLength];
                    CFrame.CopyPixelDataTo(pixelData);
                    receivedData = true;
                }
            }

            if (receivedData)
            {
                sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                BitmapSource source = BitmapSource.Create(640, 480, 96, 96,PixelFormats.Bgr32, null, pixelData, 640 * 4);

                videoImage.Source = source;
            }
        }
    }
}
