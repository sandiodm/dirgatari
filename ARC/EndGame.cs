﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Media;
using Coding4Fun.Kinect.Wpf.Controls;

namespace ARC
{
    class EndGame
    {
        public List<HoverButton> _button = new List<HoverButton>();

        public EndGame(int score, int WinState, Canvas canvas, double windowWidth, double windowHeight)
        {
            this.callPageScore(score, WinState, canvas,windowWidth,windowHeight);
            this.AddSound(WinState);
        }

        private void AddSound(int winState)
        {
            if (winState == 0)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"../../Sound/boo.wav");
                player.Play();
            } 
            else if(winState == 1)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"../../Sound/applause.wav");
                player.Play();
            }
        }

        private void callPageScore(int score, int winState, Canvas canvas, double windowWidth, double windowHeight)
        {
            if (winState == 0)
            {
                Rectangle rect = new Rectangle { Width = windowWidth, Height = 342 };
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\latar_kalah.png", UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                Canvas.SetTop(rect, windowHeight);
                canvas.Children.Add(rect);
                rect.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(rect), windowHeight - rect.Height, new Duration(TimeSpan.FromSeconds(0.6))));
                
            }
            else if (winState == 1)
            {
                Rectangle rect = new Rectangle { Width = windowWidth, Height = 590 };
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\latar_menang.png", UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                Canvas.SetTop(rect, windowHeight);
                canvas.Children.Add(rect);
                rect.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(rect), windowHeight - rect.Height, new Duration(TimeSpan.FromSeconds(0.6))));
            }
            
            this.addButton(score,winState,canvas, windowHeight, windowWidth);
        }

        private void addButton(int score, int winState, Canvas canvas, double WindowHeight, double windowWidth)
        {
            HoverButton button = new HoverButton();
            button.ImageSource = "/Images/Sprite/ulangi.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 168;
            button.Height = 56;
            Canvas.SetLeft(button, (windowWidth/2) - 50);
            Canvas.SetTop(button, 0);
            canvas.Children.Add(button);
            _button.Add(button);
            button.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(button), WindowHeight / 2 - 100, new Duration(TimeSpan.FromSeconds(1))));
            
            button = new HoverButton();
            button.ImageSource = "/Images/Sprite/keluar.png";
            button.IsTriggeredOnHover = true;
            button.ImageSize = 169;
            button.Height = 61;
            Canvas.SetLeft(button, (windowWidth / 2) - 50);
            Canvas.SetTop(button, 0);
            canvas.Children.Add(button);
            _button.Add(button);
            button.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(button), WindowHeight / 2 , new Duration(TimeSpan.FromSeconds(1))));

            if (winState == 0)
            {
                Rectangle rect = new Rectangle { Width = 327, Height = 116 };
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\papan_nilai.png", UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                Canvas.SetLeft(rect, windowWidth - rect.Width - 20);
                Canvas.SetTop(rect, 0);
                canvas.Children.Add(rect);
                rect.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(rect), 200, new Duration(TimeSpan.FromSeconds(0.2))));
                
                TextBox txt = new TextBox();
                txt.FontSize = 34;
                txt.Text = score.ToString();
                txt.FontFamily = new FontFamily("Stencil");
                txt.Background = Brushes.Transparent;
                txt.BorderBrush = Brushes.Transparent;
                if (score == 0)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 129);
                }
                else if (score > 0 && score < 1000 )
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 119);
                }
                else if (score >= 1000 && score < 9999)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 109);
                }
                else if (score >= 10000 && score < 99999)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 99);
                }
                Canvas.SetTop(txt, 0);
                txt.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(txt), 240, new Duration(TimeSpan.FromSeconds(0.4))));
                canvas.Children.Add(txt);
            }
            else if (winState == 1)
            {
                Rectangle rect = new Rectangle { Width = 327, Height = 116 };
                ImageBrush brsh = new ImageBrush();
                BitmapImage bmi = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Images\\Sprite\\papan_nilai_tertinggi.png", UriKind.Relative));
                brsh.ImageSource = bmi; brsh.Opacity = 1.0;
                rect.Fill = brsh;
                Canvas.SetLeft(rect, windowWidth - rect.Width - 20);
                Canvas.SetTop(rect, 0);
                canvas.Children.Add(rect);
                rect.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(rect), 200, new Duration(TimeSpan.FromSeconds(0.2))));

                TextBox txt = new TextBox();
                txt.FontSize = 34;
                txt.Text = score.ToString();
                txt.FontFamily = new FontFamily("Stencil");
                txt.Background = Brushes.Transparent;
                txt.BorderBrush = Brushes.Transparent;
                if (score == 0)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 129);
                }
                else if (score > 0 && score < 1000)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 119);
                }
                else if (score >= 1000 && score < 9999)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 109);
                }
                else if (score >= 10000 && score < 99999)
                {
                    Canvas.SetLeft(txt, windowWidth - rect.Width + 99);
                }
                Canvas.SetTop(txt, 0);
                txt.BeginAnimation(Canvas.TopProperty, new DoubleAnimation(Canvas.GetTop(txt), 240, new Duration(TimeSpan.FromSeconds(0.4))));
                canvas.Children.Add(txt);
            }
        }

        public int NextStep(List<double> scale)
        {
            if (scale[1] == 1)
            {
                return 2;
            }
            else if (scale[0] == 1)
            {
                return 1;
            }
            return 0;
        }
    }
}
