﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WPFSoundVisualizationLib;
using Microsoft.Win32;
using System.Xml.Serialization;

namespace ARC.Helpers
{
    class Music_Handler
    {
        private List<Models.Music> musik = new List<Models.Music>();
        private String dir;
        private NAudioEngine engine;

        public Music_Handler(string fileName)
        { 
            this.dir = Directory.GetCurrentDirectory()+"\\SongFile";
            string serializationFile = Path.Combine(dir, fileName);

            XmlSerializer ser = new XmlSerializer(musik.GetType());
            using (FileStream fs = new FileStream(serializationFile + ".bin", FileMode.Open))
            {
                musik = (List<Models.Music>)ser.Deserialize(fs);
            }
            NAudioEngine.Instance.OpenFile(serializationFile+".mp3");
            engine = NAudioEngine.Instance;
        }

        public List<Models.Music> GetListMusik 
        { 
            get { return musik; }
        }

        public NAudioEngine getEngine
        { 
            get { return engine; }
        }
    }
}
